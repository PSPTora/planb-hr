<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('month',50);
            $table->string('year',50);
            $table->string('basic',50);
            $table->string('rate',50);
            $table->string('not_of_day',50);
            $table->string('basic_salary',50);
            $table->string('gasoline',50);
            $table->string('attendance',50);
            $table->string('extra_plus',50);
            $table->string('salary_tax',50);
            $table->string('tax',50);
            $table->string('salary',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries');
    }
}
