@extends('master')
@section('style')
    <style>
        div#icon {
            margin: auto;
            max-width: 30%;
        }
        div#icon img{
            max-width:100%;
            margin-top:50px;

        }
        .login div{
            display:block;
            width:100%;
            margin:20px 0px;
        }
        .login div input{
            display:block;
            width:80%;
            border:0px;
            padding:10px;
            margin:auto;
            outline:0;
        }
        .login  button{
            display:block;
            width:80%;
            border:0px;
            padding:10px;
            margin:auto;
            outline:0;
            color:#fff;
            font-weight:bold;
            background-color:#1058b3;
        }
        div#forgot a {
            color:#fff;
            font-family:arial;
            text-decoration:none;
            font-size:12px;

        }
        div#forgot{
            text-align:center;
        }
        .bodyLogin{
            background-image:url("{{asset('img/stars2.jpg')}}");background-repeat:no-repeat;
            background-size:100%;

        }
        section.error {
            color: red;
            position: absolute;
            top: 1px;
            background: #fff;
            width: 80%;
            left: 281px;
            padding: 10px 0px;
            text-align: center;
            z-index: 100;
        }
        section:before{
            content:"";
            position:absolute;
            left: -7px;
            width:15px;
            height:15px;
            background: white;
            transform:rotate(45deg);
            top: 11px;
        }
    </style>
@stop
@section('content')
@section('class')
    class="bodyLogin"
@stop
<form action="{{route('user.updatePassword')}}" method="post" enctype="multipart/form-data">
    <div class="LoginForm">
        <div class="login" style="position:relative;max-width:300px;height:400px;margin:120px auto;background-color:#18283f;">
            <div id="icon">
                <img src="img/1493042521_lock.png"/>
            </div>
            <div  style="display:block;position: relative;">
                <input type="password" placeholder="Enter New Password" name="password" required>
                <section class="error @if($errors->first('email')) @else hidden @endif">
                    {{$errors->first('password')}}
                </section>
            </div>
            <div>
                <input type="hidden" name="_token" value="{{Session::token()}}"/>
            </div>
            <div  style="display:block;">
                <button>Reset Password</button>
            </div>
            <div style="display:block;" id="forgot">
                <a href="{{route('user.login')}}">Logon your account</a>
            </div>
        </div>
</form>
@stop