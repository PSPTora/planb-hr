@extends('master')
@section('content')
    @include('admin.header')
    <div class="container-fluid">
        <div class="row">
            @include('admin.nav')
            <div class="col-sm-10" style="overflow-x:auto;">
                <a href="{{route('user.createSalary')}}"><i class="glyphicon glyphicon-plus"></i> Add New Salary</a>
                <hr/>
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="glyphicon glyphicon-usd"></i> View Salary</div>
                    <div class="panel-body">
                        <div class="col-md-3 profile">
                            @if(!empty($salary->user))
                                <p>
                                    <img  style ="max-width:100%;" src="@if(!empty($salary->user->picture)) {{asset('uploads/')}}/{{$salary->user->picture}} @else {{asset('img/1493107915_user.png')}} @endif"/>
                                    <b>{{$salary->user->name}}</b></p>
                                <p><b>Register Date</b> : {{$salary->user->created_at}}</p>
                                <p><b>Address</b> : {{$salary->user->address}}</p>
                                <p><b>Date of Birth</b> : {{$salary->user->dob}}</p>
                                <p><b>Sex</b> : {{$salary->user->sex}}</p>
                                <p><b>Email</b> : {{$salary->user->email}}</p>
                                <p><b>Phone</b> : {{$salary->user->phone}}</p>

                            @endif
                        </div>
                        <div class="col-md-9">
                            <table class="table table-bordered">
                                <tr>
                                    <thead>
                                    <tr>
                                        <th>Month</th>
                                        <td>{{$salary->month}}</td>
                                    </tr>
                                    <tr>
                                        <th>Year</th>
                                        <td>{{$salary->year}}</td>
                                    </tr>
                                    <tr>
                                        <th>Basic</th>
                                        <td>${{$salary->basic}}</td>
                                    </tr>

                                    <tr>
                                        <th>Rate</th>
                                        <td>{{$salary->rate}}</td>
                                    </tr>
                                    <tr>
                                        <th>No of Day</th>
                                        <td>{{$salary->no_of_day}}</td>
                                    </tr>
                                    <tr>
                                        <th>Basic Salary</th>
                                        <td>${{$salary->basic_salary}}</td>
                                    </tr>
                                    <tr>
                                        <th>Gasoline</th>
                                        <td>{{$salary->gasoline}}</td>
                                    </tr>
                                    <tr>
                                        <th>Attendance</th>
                                        <td>{{$salary->attendance}}</td>
                                    </tr>
                                    <tr>
                                        <th>Extra Plus</th>
                                        <td>{{$salary->extra_plus}}</td>
                                    </tr>
                                    <tr>
                                        <th>Minus</th>
                                        <td>{{$salary->minus}}</td>
                                    </tr>
                                    <tr>
                                        <th>Salary Tax</th>
                                        <td>{{$salary->salary_tax}}</td>
                                    </tr>
                                    <tr>
                                        <th>Tax</th>
                                        <td>{{$salary->tax}}</td>
                                    </tr>
                                    <tr>
                                        <th>Salary</th>
                                        <td>${{$salary->salary}}</td>
                                    </tr>
                                    </thead>
                                </tr>
                            </table>
                            <tbody>
                            @if(!empty($salary))
                            @endif
                            </tbody>
                        </div>
                    </div>
                </div>
                <!--/col-span-9-->
            </div>
        </div>

        <script type="text/javascript"
                src="{{asset('bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
        <script>
            $(".choseMode label input").click(function () {
                var attr = $(this).attr('id');
                if (attr == 'text') {
                    $("#fileCsv").addClass('hidden');
                    $("#input").removeClass('hidden');

                } else if (attr == 'file') {
                    $("#input").addClass('hidden');
                    $("#fileCsv").removeClass('hidden');
                }
            });
            $("#form").validate({
                rules: {
                    basic_salary: {
                        required: true
                    },
                    month: {
                        required: true
                    },
                    gasoline: {
                        required: true
                    }
                }
            });
        </script>
        @if($errors->first('file'))
            <script type="text/javascript">
                $("#input").addClass('hidden');
                $("#fileCsv").removeClass('hidden');
            </script>
        @endif
        @if($errors->first('error'))
            <script type="text/javascript">
                $("#input").addClass('hidden');
                $("#fileCsv").removeClass('hidden');
            </script>
        @endif
        <style type="text/css">
            label.error {
                color: #F44336;
            }
        </style>
@stop