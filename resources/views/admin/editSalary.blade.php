@extends('master')
@section('content')
	@include('admin.header')
	<div class="container-fluid">
	    <div class="row">
			@include('admin.nav')
			<div class="col-sm-10" style="overflow-x:auto;">
            <a href="{{route('user.createSalary')}}"><i class="glyphicon glyphicon-plus"></i> Add New Salary</a>
            <hr/>
            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-leaf"></i> Edit Salary</div>
				<br>
				<div class="col-md-12">
					@include('admin.message')
				</div>
                <div class="panel-body">
                    <div id="input">
	                    <form action="{{route('user.updateSalary')}}" method="post" enctype="multipart/form-data" id="form">
	                    <input type="hidden" name="_token" value="{{Session::token()}}">
	                        <div class="col-md-12">
	                            <div class="form-group">
	                                <label for="">Select User</label>
	                                <select class="form-control" name="user_id">
	                                    @if(!empty($users))
											@foreach($users as $user)
												<option value="{{$user->id}}" @if($user->id==@$_GET['user_id']) selected @endif>{{$user->name}}</option>
											@endforeach
	                                    @endif
	                                </select>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
									<input type="hidden" name="id" value="{{$salary->id}}">
	                                <label>Month</label>
	                                <input type="text" class="form-control" value="{{$salary->month}}" name="month" id="month">
	                                <span class="text-danger">{{$errors->first('month')}}</span>

	                            </div>
	                        </div>

	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Rate</label>
	                                <input type="text" class="form-control" value="{{$salary->rate}}" name="rate" >
	                                <span class="text-danger">{{$errors->first('text')}}</span>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">No of Day</label>
	                                <input type="text" class="form-control" style="padding:3px 12px;" name="no_of_day" value="{{$salary->no_of_day}}">
	                            </div>
	                        </div>
							<div class="clearfix"></div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Year</label>
									<input type="text" class="form-control" value="{{$salary->year}}" name="year" id="year">
									<span class="text-danger">{{$errors->first('month')}}</span>

								</div>
							</div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Basic Salary</label>
	                                <input type="text" class="form-control"  value="{{$salary->basic_salary}}"  name="basic_salary" id="basic_salary">
	                                <span class="text-danger">{{$errors->first('basic_salary')}}</span>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Gasoline</label>
	                                <input type="text" name="gasoline" id="gasoline" class="form-control" value="{{$salary->gasoline}}">
	                            	<span class="text-danger">{{$errors->first('gasoline')}}</span>
	                            </div>
	                        </div>
							<div class="clearfix"></div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Attendance</label>
	                                <input type="text" class="form-control" value="{{$salary->attendance}}" name="attendance" id="Attendance">
	                            </div>
	                        </div>


	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Extra Plus</label>
	                                <input type="text" class="form-control" value="{{$salary->extra_plus}}" name="extra_plus">
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Minus</label>
	                                <input type="text" class="form-control" value="{{$salary->minus}}"  name="minus" id="minus">
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Salary (W/O) Tax</label>
	                                <input type="text" class="form-control" style="padding:3px 12px;" name="salary_tax" value="{{$salary->salary_tax}}"  id="salary_tax">
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <label for="">Tax</label>
	                            <input type="text" name="tax" id="tax" class="form-control" value="{{$salary->tax}}" >
	                        </div>
	                        <div class="col-md-4">
	                            <label for="">Salary</label>
	                            <input type="text" name="salary" id="" class="form-control" value="{{$salary->salary}}">
	                            <span class="text-danger">{{$errors->first('salary')}}</span>
	                        </div>
	                        <div class="clearfix"></div>
	                        <br>
	                        <div class="col-md-12">
	                            <button class="btn btn-success"><i class="glyphicon glyphicon-leaf"></i> Update</button>
	                        </div>

                    </form>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
        <!--/col-span-9-->
		</div>
	</div>

<script type="text/javascript" src="{{asset('bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script>
    $(".choseMode label input").click(function () {
        var attr = $(this).attr('id');
        if (attr == 'text') {
            $("#fileCsv").addClass('hidden');
            $("#input").removeClass('hidden');

        } else if (attr == 'file') {
            $("#input").addClass('hidden');
            $("#fileCsv").removeClass('hidden');
        }
    });
    $( "#form" ).validate({
	  rules: {
	    basic_salary: {
	      required: true
	    },
	    month:{
	      required: true
	    },
	    gasoline:{
	      required: true
	    }
	  }
	});
</script>
@if($errors->first('file'))
<script type="text/javascript">
	$("#input").addClass('hidden');
    $("#fileCsv").removeClass('hidden');
</script>
@endif
@if($errors->first('error'))
<script type="text/javascript">
	$("#input").addClass('hidden');
    $("#fileCsv").removeClass('hidden');
</script>
@endif
<style type="text/css">
	label.error {
    	color:#F44336;
	}
</style>
@stop