@extends('master')
@section('content')
	@include('admin.header')
	<div class="container-fluid">
	    <div class="row">
			@include('admin.nav')
			<div class="col-sm-10" style="overflow-x:auto;">
            <a href="/role-create"><i class="glyphicon glyphicon-plus"></i> Add New Role</a>
            <hr/>
            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-user"></i>Role</div>
                <div class="panel-body">
                 @include('admin.message')
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width:150px;">Role Name</th>
                                <th>Description</th>
                                <th style="width:250px;">Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($roles))
								@foreach($roles as $role)
								<tr>
                                    <td>{{$role->name}}</td>
                                    <td>{{$role->description}}</td>
                                    <td>
                                         <a href="{{route('user.roleEdit',['id'=>$role->id])}}" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-edit"></i> Edit</a>

                                        |

                                            <a  class="btn btn-danger btn-xs" href="{{route('user.roleDelete',['id'=>$role->id])}}" onclick="return confirm('Are you sure to deleted?')"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                                        |

                                           <a href="{{route('user.roleView',['id'=>$role->id])}}" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-eye-open"></i> View</a>

                                    </td>
                                </tr>

								@endforeach
                            @endif




                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
        <!--/col-span-9-->
		</div>
	</div>
@stop