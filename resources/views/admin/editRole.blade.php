@extends('master')
@section('content')
	@include('admin.header')
	<div class="container-fluid">
	    <div class="row">
			@include('admin.nav')
			<div class="col-sm-10" style="overflow-x:auto;">
            <a href="/role-create"><i class="glyphicon glyphicon-plus"></i> Add New Role</a>
            <hr/>
            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-user"></i> Edit Role</div>
                <div class="panel-body">
                    @include('admin.message')
                    <form action="{{route('user.updateRole')}}" method="post" enctype="multipart/form-data">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="hidden" name="_token" value="{{Session::token()}}">
                                <label for="">Role Name</label>
                                <input type="text" name="name" id="" class="form-control" value="{{$role->name}}">
                                <input type="hidden" name="id" value="{{$role->id}}">
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <textarea name="description" id="" cols="30" rows="5" class="form-control">{{$role->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i> Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>
        <!--/col-span-9-->
		</div>
	</div>
@stop