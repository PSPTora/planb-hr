@extends('master')
@section('content')
	@include('admin.header')

	@if($errors->first('Exists'))
  		<script type="text/javascript">
  			Command: toastr["error"]("{!!$errors->first('Exists')!!} is already exist in database", "Errors")
			toastr.options = {
			  "closeButton": false,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": false,
			  "positionClass": "toast-top-full-width",
			  "preventDuplicates": false,
			  "onclick": null,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
  		</script>
    @endif
    @if($errors->first('Adds'))
  		<script type="text/javascript">
  			Command: toastr["success"]("{!!$errors->first('Adds')!!} has been added to database", "Success")
			toastr.options = {
			  "closeButton": false,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": false,
			  "positionClass": "toast-top-full-width",
			  "preventDuplicates": false,
			  "onclick": null,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
  		</script>
    @endif
	<div class="container-fluid">
	    <div class="row">
			@include('admin.nav')
			<div class="col-sm-10" style="overflow-x:auto;">
            <a href="{{route('add.user')}}"><i class="glyphicon glyphicon-plus"></i> Add New User</a>
            <hr/>
            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-user"></i> Add New User</div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="form-group choseMode">
                            <h3>Choose Mode</h3>
                            <hr>
                            <label for=""><input type="radio" name="mode" id="text" data="text"><i
                                    class="glyphicon glyphicon-user"></i> User Input</label>
                            <label for=""><input type="radio" name="mode" id="file" data="file"> <i
                                    class="glyphicon glyphicon-file"></i> CSV File</label>
                            <hr>
                        </div>
                    </div>
                    <div id="input">
                    	@include('admin.message')
	                    <form action="{{route('add.user')}}" method="post" enctype="multipart/form-data">
	                    <input type="hidden" name="_token" value="{{Session::token()}}">
	                        <div class="col-md-12">
	                            <div class="form-group">
	                                <label for="">Choose Role</label>
	                                <select class="form-control" name="role_id">
	                                    @if(!empty($roles))
											@foreach($roles as $role)
												<option value="{{$role->id}}">{{$role->name}}</option>
											@endforeach
	                                    @endif
	                                </select>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label>Enter user name*</label>
	                                <input type="text" class="form-control" placeholder="Enter user name" name="name" required>
	                  
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Enter email address*</label>
	                                <input type="text" class="form-control" placeholder="Enter email address" name="email" required>
	                                <span class="text-danger">{{$errors->first('email')}}</span>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Enter password*</label>
	                                <input type="password" class="form-control" style="padding:3px 12px;" name="password" required>
	                            </div>
	                        </div>
							<div class="clearfix"></div>

	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Enter phone number</label>
	                                <input type="text" class="form-control" placeholder="Enter phone number" name="phone">
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Select Sex</label>
	                                <select name="sex" class="form-control" name="sex">
	                                    <option value="">Select Sex</option>
	                                    <option value="Male">Male</option>
	                                    <option value="Female">Female</option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Enter date of birth</label>
	                                <input type="date" class="form-control" placeholder="Enter date of birth" name="dob">
	                            </div>
	                        </div>


	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Enter place of birth</label>
	                                <input type="text" class="form-control" placeholder="Enter place of birth" name="pob">
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Enter your address</label>
	                                <input type="text" class="form-control" placeholder="Enter your address" name="address">
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <div class="form-group">
	                                <label for="">Choose picture</label>
	                                <input type="file" class="form-control" style="padding:3px 12px;" name="picture">
	                            </div>
	                        </div>
	                        <div class="col-md-4">
	                            <label for="">Status</label>
	                            <select class="form-control" name="status">
	                                <option value="1">Active</option>
	                                <option value="0">Decative</option>

	                            </select>
	                        </div>
	                        <div class="clearfix"></div>
	                        <br>
	                        <div class="col-md-12">
	                            <button class="btn btn-success"><i class="glyphicon glyphicon-user"></i> Add New</button>
	                        </div>
	                    </div>
                    </form>
                    <div class="clearfix"></div>
                    <div id="fileCsv" class="hidden">
                    	<form action="{{route('addUserCSV')}}" method="post" enctype="multipart/form-data">
	                        <div class="form-group">
	                            <label for="">Choose File</label>
	                            <input type="file" class="form-control" style="padding:3px 12px;" name="file">
	                            <input type="hidden" value="{{Session::token()}}" name="_token">
	                        </div>
	                        <div class="col-md-12">
	                            <button class="btn btn-success"><i class="glyphicon glyphicon-file"></i> Upload FIle
	                            </button>

	                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--/col-span-9-->
		</div>
	</div>
	<script>
    $(".choseMode label input").click(function () {
        var attr = $(this).attr('id');
        if (attr == 'text') {
            $("#fileCsv").addClass('hidden');
            $("#input").removeClass('hidden');

        } else if (attr == 'file') {
            $("#input").addClass('hidden');
            $("#fileCsv").removeClass('hidden');
        }
    })
</script>
@stop