<!-- header -->
<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" @if(!empty(Auth::user()->picture))  style="margin-top:10px;" @endif>Plan-B HR</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">@if(!empty(Auth::user()->picture))
                            <img src="{{asset('uploads')}}/{{Auth::user()->picture}}" alt="" width="30" style="border-radius:50%;"> @else <i class="glyphicon glyphicon-user"></i>@endif {{Auth::user()->name}} <span class="caret"></span></a>
                    <ul id="g-account-menu" class="dropdown-menu" role="menu">
                        <li><a href="@if(Auth::check()) {{route('user.update.profile',['id'=>Auth::user()->id])}} @endif">My Profile</a></li>
                    </ul>
                </li>
                <li @if(!empty(Auth::user()->picture)) style="margin-top:0px;" @endif><a href="{{route('user.logout')}}"><i class="glyphicon glyphicon-lock"></i> Logout</a></li>
            </ul>
        </div>
    </div>
    <!-- /container -->
</div>
<!-- /Header -->