@extends('master')
@section('content')
    @include('admin.header')
    <div class="container-fluid">
        <div class="row">
            @include('admin.nav')
            <div class="col-sm-10" style="overflow-x:auto;">
                <a href="{{route('user.createSalary')}}"><i class="glyphicon glyphicon-plus"></i> Add New Salary</a>
                <hr/>
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="glyphicon glyphicon-usd"></i> Add New Salary</div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="form-group choseMode">
                                <h3>Choose Mode</h3>
                                <hr>
                                <label for=""><input type="radio" name="mode" id="text" data="text"><i
                                            class="glyphicon glyphicon-user"></i> User Input</label>
                                <label for=""><input type="radio" name="mode" id="file" data="file"> <i
                                            class="glyphicon glyphicon-file"></i> CSV File</label>
                                <hr>
                            </div>
                        </div>
                        <div id="input">
                            <div class="clearfix"></div>
                            @php $userNotFound=$errors->first('notFound'); @endphp
                            @if($userNotFound!=="")
                                <?php  $userNotFound = rtrim($userNotFound, "|");?>
                                @if($userNotFound)
                                    <div class="alert alert-danger">
                                        <p>User doesn't exists in system</p>
                                        @php
                                            $userArray=explode("|",$userNotFound);
                                        @endphp
                                        @foreach($userArray as $notFoundUser)
                                            <li>{{$notFoundUser}}</li>
                                        @endforeach
                                    </div>
                                @endif
                            @endif
                            <div class="clearfix"></div>
                            @include('admin.message')
                            @php $userNotFound=$errors->first('userFound'); @endphp
                            @if($userNotFound!=="")
                                <?php  $userNotFound = rtrim($userNotFound, "|");?>
                                @if($userNotFound)
                                    <div class="alert alert-success">
                                        <p>Users has been updated</p>
                                        @php
                                            $userArray=explode("|",$userNotFound);
                                        @endphp
                                        @foreach($userArray as $notFoundUser)
                                            <li>{{$notFoundUser}}</li>
                                        @endforeach
                                    </div>
                                @endif
                            @endif

                            <form action="{{route('user.createSalary')}}" method="post"
                                  enctype="multipart/form-data"
                                  id="form">
                                <input type="hidden" name="_token" value="{{Session::token()}}">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Select User</label>
                                        <select class="form-control" name="user_id">
                                            @if(!empty($users))
                                                @foreach($users as $user)
                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Month</label>
                                        <input type="date" class="form-control" placeholder="Enter Month"
                                               name="month"
                                               id="month">
                                        <span class="text-danger">{{$errors->first('month')}}</span>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Rate</label>
                                        <input type="text" class="form-control" placeholder="Enter Rate"
                                               name="rate">
                                        <span class="text-danger">{{$errors->first('text')}}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">No of Day</label>
                                        <input type="text" class="form-control" style="padding:3px 12px;"
                                               name="no_of_day">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Basic Salary</label>
                                        <input type="text" class="form-control" placeholder="Enter basic salary"
                                               name="basic_salary" id="basic_salary">
                                        <span class="text-danger">{{$errors->first('basic_salary')}}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Gasoline</label>
                                        <input type="text" name="gasoline" id="gasoline" class="form-control"
                                               placeholder="Enter Gasoline">
                                        <span class="text-danger">{{$errors->first('gasoline')}}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Attendance</label>
                                        <input type="text" class="form-control" placeholder="Enter Attendance"
                                               name="attendance" id="Attendance">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Extra Plus</label>
                                        <input type="text" class="form-control" placeholder="Enter Extra Plus"
                                               name="extra_plus">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Minus</label>
                                        <input type="text" class="form-control" placeholder="Enter Minus"
                                               name="minus"
                                               id="minus">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Salary (W/O) Tax</label>
                                        <input type="text" class="form-control" style="padding:3px 12px;"
                                               name="salary_tax" placeholder="Salary Tax" id="salary_tax">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="">Tax</label>
                                    <input type="text" name="tax" id="tax" class="form-control"
                                           placeholder="Enter tax">
                                </div>
                                <div class="col-md-4">
                                    <label for="">Salary</label>
                                    <input type="text" name="salary" id="" class="form-control"
                                           placeholder="Enter salary">
                                    <span class="text-danger">{{$errors->first('salary')}}</span>
                                </div>
                                <div class="clearfix"></div>
                                <br>
                                <div class="col-md-12">
                                    <button class="btn btn-success"><i class="glyphicon glyphicon-leaf"></i> Add
                                        New
                                    </button>
                                </div>
                        </div>
                        </form>
                        <div class="clearfix"></div>
                        <div id="fileCsv" class="hidden">
                            <form action="{{route('salary.csv')}}" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="">Choose File</label>
                                    <input type="file" class="form-control" style="padding:3px 12px;" name="file">
                                    <input type="hidden" value="{{Session::token()}}" name="_token">
                                    <label class="error">{{$errors->first('file')}}</label>
                                    <label class="error">{{$errors->first('error')}}</label>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success"><i class="glyphicon glyphicon-file"></i> Upload FIle
                                    </button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--/col-span-9-->
        </div>
    </div>

    <script type="text/javascript"
            src="{{asset('bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script>
        $(".choseMode label input").click(function () {
            var attr = $(this).attr('id');
            if (attr == 'text') {
                $("#fileCsv").addClass('hidden');
                $("#input").removeClass('hidden');

            } else if (attr == 'file') {
                $("#input").addClass('hidden');
                $("#fileCsv").removeClass('hidden');
            }
        });
        $("#form").validate({
            rules: {
                basic_salary: {
                    required: true
                },
                month: {
                    required: true
                },
                gasoline: {
                    required: true
                }
            }
        });
    </script>
    @if($errors->first('file'))
        <script type="text/javascript">
            $("#input").addClass('hidden');
            $("#fileCsv").removeClass('hidden');
        </script>
    @endif
    @if($errors->first('error'))
        <script type="text/javascript">
            $("#input").addClass('hidden');
            $("#fileCsv").removeClass('hidden');
        </script>
    @endif
    <style type="text/css">
        label.error {
            color: #F44336;
        }
    </style>
@stop