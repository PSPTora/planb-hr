@extends('master')
@section('content')
    @include('admin.header')
    <div class="container-fluid">
        <div class="row">
            @include('admin.nav')
            <div class="col-sm-10" style="overflow-x:auto;">
                <a href="{{route('add.user')}}"><i class="glyphicon glyphicon-plus"></i> Add New User</a>
                <hr/>
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="glyphicon glyphicon-user"></i>User</div>
                    <div class="panel-body">
                        @include('admin.message')
                        <div class="col-md-12" style="padding-left:0px;">
                            <form action="{{route('user.searchUser')}}" method="get" enctype="multipart/form-data">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Seaech ....   " name="search">
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table" style="width: 1500px;">
                                <thead>
                                <tr>
                                    <th style="width: 300px;">Action</th>
                                    <th>Name</th>
                                    <th>Permission</th>
                                    <th>Email</th>
                                    <th>Sex</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Picture</th>
                                    <th>Date of Birth</th>
                                    <th>Place of Birth</th>
                                    <th>Status</th>

                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($users))
                                    @foreach($users as $user)
                                        @if(Auth::user()->id!=$user->id)
                                            <tr>
                                                <td>
                                                    <a href="{{route('user.edit',['id'=>$user->id])}}" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;
                                                    <a href="{{route('delete.user',['id'=>$user->id])}}" onclick="return confirm('Are you sure to delete?')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Delete</a>&nbsp;
                                                    <a href="{{route('user.status',['id'=>$user->id])}}" class="btn btn-success btn-xs"><i class="@if($user->status=='1') 	glyphicon glyphicon-ok @else glyphicon glyphicon-remove @endif"></i>@if($user->status=="1") Enabled&nbsp; @else Disabled @endif</a>
                                                </td>
                                                <td>{{$user->name}}</td>
                                                <td>
                                                    @if(!empty($user->role))
                                                        {{$user->role->name}}
                                                    @else
                                                        User
                                                    @endif
                                                </td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->sex}}</td>
                                                <td>{{$user->phone}}</td>
                                                <td>{{$user->address}}</td>
                                                <td>@if(!empty($user->picture)) <img
                                                            src="{{asset('uploads')}}/{{$user->picture}}"
                                                            style="width:50px;border-radius: 50%;"/> @else <img
                                                            src="img/1493107915_user.png"/> @endif</td>
                                                <td>{{$user->dob}}</td>
                                                <td>{{$user->pob}}</td>
                                                <td>@if($user->status=="1") Active @else Deactive @endif</td>

                                            </tr>
                                        @endif
                                    @endforeach
                                @endif


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
            <!--/col-span-9-->
        </div>
    </div>
@stop