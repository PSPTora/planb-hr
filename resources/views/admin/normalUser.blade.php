@extends('master')
@section('content')
    @include('admin.headerUser')
    <div class="container-fluid">
        <div class="row">
            @include('admin.navUser')
            <div class="col-sm-10" style="overflow-x:auto;">

                <div class="panel panel-default">
                    <div class="panel-heading"><i class="glyphicon glyphicon-usd"></i>Salary</div>
                    <div class="panel-body">
                        @include('admin.message')
                        <div class="table-responsive col-md-6">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Month</th>
                                    <th>Year</th>
                                    <th>Salary</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(Auth::check())
                                    <?php $total=0;?>
                                    @if(!empty(Auth::user()->salaries))
                                        @foreach(Auth::user()->salaries as $salary)
                                            <tr>
                                                <td>{{$salary->month}}</td>
                                                <td>{{$salary->year}}</td>
                                                <td>${{$salary->salary}}</td>
                                                <?php $total=$total+$salary->salary;?>
                                                <td>
                                                    <a href="{{route('view.salary.user.normal',['id'=>$salary->id])}}" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="4" align="right">Total ${{$total}}</td>
                                        </tr>
                                    @endif
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
            <!--/col-span-9-->
        </div>
    </div>
    <div>

    </div>
@stop