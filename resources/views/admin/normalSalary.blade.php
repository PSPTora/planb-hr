@extends('master')
@section('content')
    @include('admin.headerUser')
    <div class="container-fluid">
        <div class="row">
            @include('admin.navUser')
            <div class="col-sm-10" style="overflow-x:auto;">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="glyphicon glyphicon-leaf"></i>Salary</div>
                    <div class="panel-body">
                        @include('admin.message')
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Month</th>
                                    <th>Year</th>
                                    <th>Basic</th>
                                    <th>Rate</th>
                                    <th>Basic Salary</th>
                                    <th>Gasoline</th>
                                    <th>Attendance</th>
                                    <th>Extra Plus</th>
                                    <th>Minus</th>
                                    <th>Salary Tax</th>
                                    <th>Tax</th>
                                    <th>Salary</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($salary))
                                    <tr>
                                        <td>{{$salary->month}}</td>
                                        <td>{{$salary->year}}</td>
                                        <td>${{$salary->basic}}</td>
                                        <td>{{$salary->rate}}</td>
                                        <td>{{($salary->basic_salary)?"$".$salary->basic_salary:""}}</td>
                                        <td>{{($salary->gasoline)?"$".$salary->gasoline:""}}</td>
                                        <td>{{($salary->attendance)?"$".$salary->attendance:""}}</td>
                                        <td>{{$salary->extra_plus}}</td>
                                        <td>{{$salary->minus}}</td>
                                        <td>{{$salary->salary_tax}}</td>
                                        <td>{{$salary->tax}}</td>
                                        <td>${{$salary->salary}}</td>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
            <!--/col-span-9-->
        </div>
    </div>
    <div>

    </div>
@stop