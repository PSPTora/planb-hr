@extends('master')
@section('content')
	@include('admin.header')
	<div class="container-fluid">
	    <div class="row">
			@include('admin.nav')
			<div class="col-sm-10" style="overflow-x:auto;">
            <a href="/role-create"><i class="glyphicon glyphicon-plus"></i> Add New Role</a>
            <hr/>
            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-user"></i>  Role View</div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="width:150px;">Role Name</th>
                                <th>Description</th>
                                <th style="width:200px;">Created_at</th>
                                <th style="width:200px;">Updated_at</th>
                          
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($role))
								<tr>
                                    <td>{{$role->name}}</td>                        
                                    <td>{{$role->description}}</td>                        
                                    <td>{{$role->created_at}}</td>                        
                                    <td>{{$role->updated_at}}</td>                        
                                </tr>
                            @endif
                            
                           
                            

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
        <!--/col-span-9-->
		</div>
	</div>
@stop