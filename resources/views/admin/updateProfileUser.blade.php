@extends('master')
@section('content')
    @include('admin.headerUser')
    <div class="container-fluid">
        <div class="row">
            @include('admin.navUser')
            <div class="col-sm-10" style="overflow-x:auto;">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="glyphicon glyphicon-user"></i> Edit User</div>
                    <div class="panel-body">
                        <div id="input">

                            <form action="{{route('user.update.profiles')}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{Session::token()}}">
                                <div class="col-md-12">
                                    @include('admin.message')
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Enter user name</label>
                                        <input type="text" class="form-control" placeholder="Enter user name" name="name" required value="{{$user->name}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Enter email address</label>
                                        <input type="text" class="form-control" placeholder="Enter email address" name="email" value="{{$user->email}}" required>
                                        <span class="text-danger">{{$errors->first('email')}}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Enter password</label>
                                        <input type="password"  required class="form-control" style="padding:3px 12px;" name="password">
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Enter phone number</label>
                                        <input type="text" class="form-control" placeholder="Enter phone number" name="phone" value="{{$user->phone}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Select Sex</label>
                                        <select name="sex" class="form-control" name="sex">
                                            <option value="">Select Sex</option>
                                            <option value="Male" @if($user->sex=="Male")  selected @endif>Male</option>
                                            <option value="Female" @if($user->sex=="Female") selected  @endif>Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Enter date of birth</label>
                                        <input type="date" class="form-control" placeholder="Enter date of birth" name="dob" value="{{$user->dob}}">
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Enter place of birth</label>
                                        <input type="text" class="form-control" placeholder="Enter place of birth" name="pob" value="{{$user->pob}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Enter your address</label>
                                        <input type="text" class="form-control" placeholder="Enter your address" name="address" value="{{$user->address}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Choose picture</label>
                                        <input type="file" class="form-control" style="padding:3px 12px;" name="picture">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <br>
                                <div class="col-md-12">
                                    <button class="btn btn-success"><i class="glyphicon glyphicon-user"></i> Update</button>
                                </div>
                                <input type="hidden" name="id" value="{{$user->id}}">

                            </form>
                        </div>
                    </div>
                </div>
                <!--/col-span-9-->
            </div>
        </div>
        <script>
            $(".choseMode label input").click(function () {
                var attr = $(this).attr('id');
                if (attr == 'text') {
                    $("#fileCsv").addClass('hidden');
                    $("#input").removeClass('hidden');

                } else if (attr == 'file') {
                    $("#input").addClass('hidden');
                    $("#fileCsv").removeClass('hidden');
                }
            })
        </script>
@stop