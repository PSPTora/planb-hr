@extends('master')
@section('content')
    @include('admin.header')
    <div class="container-fluid">
        <div class="row">
            @include('admin.nav')
            <div class="col-md-10">
                <div id="input">
                    <form action="{{route('user.role')}}" method="post" enctype="multipart/form-data">
                        <div class="col-md-12" style="padding-left:0px;">
                            <div class="form-group">
                                <label>Role Name</label>
                                <input type="text" class="form-control" placeholder="Enter role name" required=""
                                       name="name">
                                <span class="text-danger">{{$errors->first('name')}}</span>
                            </div>
                            <input type="hidden" name="_token" value="{{Session::token()}}">
                        </div>
                        <div class="col-md-12" style="padding-left:0px;">
                            <div class="form-group">
                                <label>Role Description</label>
                                <input type="text" class="form-control" placeholder="Description " required=""
                                       name="description">
                                <span class="text-danger">{{$errors->first('description')}}</span>
                            </div>

                        </div>
                        <div class="col-md-12" style="padding-left: 0px;">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
                <hr>

            </div>
        </div>
    </div>
@stop