@if($errors->first('notice'))
	<div class="alert alert-success" style="clear:both;">
		{{$errors->first('notice')}}
	</div>
@endif
@if($errors->first('error'))
	<div class="alert alert-danger" style="clear:both;">
		{{$errors->first('error')}}
	</div>
@endif
<div class="clearfix"></div>