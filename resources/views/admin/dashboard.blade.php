@extends('master')
@section('content')
	@include('admin.header')
	<div class="container-fluid">
	    <div class="row">
			@include('admin.nav')
			<div class="col-md-10">
				<div class="list">
					<div class="page-heading">
						<div class="alert alert-success">
							<h4 style="margin:0px;padding:0px;">Welcome <b style="text-transform: capitalize;"> {{Auth::user()->name}}</b></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop