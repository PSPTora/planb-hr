@extends('master')
@section('content')
    @include('admin.header')
    <div class="container-fluid">
        <div class="row">
            @include('admin.nav')
            <div class="col-sm-10" style="overflow-x:auto;">
                <a href="{{route('user.createSalary')}}"><i class="glyphicon glyphicon-plus"></i> Add New Salary</a>
                <hr/>
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="glyphicon glyphicon-leaf"></i> Salary This Month</div>
                    <div class="col-md-12">
                        <br>
                        @include('admin.message')
                    </div>
                    <div class="panel-body">
                        <div id="input">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th width="200">Name</th>
                                        <th width="200">Month</th>
                                        <th width="200">Year</th>
                                        <th width="200">Salary</th>
                                        <th>Acton</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($salaries))
                                        @foreach($salaries as $salary)
                                            <tr>
                                                <td><a href="{{route('user.salary.detail',['id'=>$salary->user->id])}}">{{ucwords($salary->user->name)}}</a></td>
                                                <td>{{$salary->month}}</td>
                                                <td>{{$salary->year}}</td>
                                                <td>${{$salary->salary}}</td>
                                                <td>

                                                        <a href="{{route('user.edit.salary',['id'=>$salary->id,'user_id'=>$salary->user->id])}}" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                                        <a  class="btn btn-danger btn-xs"  href="{{route('user.delete.salary',['id'=>$salary->id])}}" onclick="return confirm('Are you sure to delete')"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                                                        <a href="{{route('user.view.salary',['id'=>$salary->id])}}" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-eye-open"></i> View</a>

                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                                @if(!empty($salaries))
                                    {{$salaries->render()}}
                                @endif
                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <div id="fileCsv" class="hidden">
                            <div class="form-group">
                                <label for="">Choose File</label>
                                <input type="file" class="form-control" style="padding:3px 12px;">
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-success"><i class="glyphicon glyphicon-file"></i> Upload FIle
                                </button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!--/col-span-9-->
        </div>
    </div>
    <?php
        function getSalary($userID){
            return \App\Models\Salary::orderBy('id','desc')->first();
        }

    ?>
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>
    <script>
        $(".choseMode label input").click(function () {
            var attr = $(this).attr('id');
            if (attr == 'text') {
                $("#fileCsv").addClass('hidden');
                $("#input").removeClass('hidden');

            } else if (attr == 'file') {
                $("#input").addClass('hidden');
                $("#fileCsv").removeClass('hidden');
            }
        });

        $(".select").select2({
            placeholder: "Select User"
        });
        $(".select1").select2({
            placeholder: ""
        });
        $(".selectOption").select2({
            placeholder: ""
        });
    </script>
@stop