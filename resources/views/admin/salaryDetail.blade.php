@extends('master')
@section('content')
    @include('admin.header')
    <div class="container-fluid">
        <div class="row">
            @include('admin.nav')
            <div class="col-sm-10" style="overflow-x:auto;">
                <a href="{{route('user.createSalary')}}"><i class="glyphicon glyphicon-plus"></i> Add New Salary</a>
                <hr/>
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="glyphicon glyphicon-user"></i>User Details</div>
                    <div class="panel-body">
                        @if(!empty($user))
                            <p><img style="max-width:60px;border-radius: 50%;"
                                    src="@if(!empty($user->picture)) {{asset('uploads')}}/{{$user->picture}} @else{{asset('img/1493107915_user.png')}} @endif"
                                    alt=""> {{$user->name}}</p>
                            <hr>
                            @if(!empty($user->salaries))
                                <div class="table-responsive">
                                    <table class="table" style="width:2000px;">
                                        <thead>
                                        <tr>
                                            <th>Action</th>
                                            <th width="200">Name</th>
                                            <th width="200">Month</th>
                                            <th width="200">Year</th>
                                            <th width="200">Basic</th>
                                            <th width="200">Rate</th>
                                            <th width="200">No Of Day</th>
                                            <th width="200">Basic Salary</th>
                                            <th width="200">Gasoline</th>
                                            <th width="200">Attendance</th>
                                            <th width="200">Extra Plus</th>
                                            <th width="200">Minus</th>
                                            <th width="200">Salary Tax</th>
                                            <th width="200">Tax</th>
                                            <th width="200">Salary</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php $total = 0;?>
                                        @foreach($user->salaries as $salary)
                                            <tr>
                                                <td width="450">
                                                    <a href="{{route('user.delete.salary',['id'=>$salary->id])}}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure to delete?')"> <i class="glyphicon glyphicon-trash"></i> Delete</a>
                                                    &nbsp;
                                                    <a href="{{route('user.edit.salary',['id'=>$salary->id,'user_id'=>$salary->user_id])}}" class="btn btn-success btn-xs"> <i class="glyphicon glyphicon-edit"></i> edit</a>
                                                    &nbsp;
                                                    <a href="{{route('user.view.salary' ,['id'=>$salary->id])}}" class="btn btn-info btn-xs"> <i class="glyphicon glyphicon-eye-open"></i> View</a>
                                                </td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$salary->month}}</td>
                                                <td>{{$salary->year}}</td>
                                                <td>${{$salary->basic}}</td>
                                                <td>{{$salary->rate}}</td>
                                                <td>{{$salary->no_of_day}}</td>
                                                <td>${{$salary->basic_salary}}</td>
                                                <td>{{$salary->gasoline}}</td>
                                                <td>${{$salary->attendance}}</td>
                                                <td>{{$salary->extra_plus}}</td>
                                                <td>{{$salary->minus}}</td>
                                                <td>{{$salary->salary_tax}}</td>
                                                <td>{{$salary->tax}}</td>
                                                <td>${{$salary->salary}}</td>
                                                <?php $total = $total + $salary->salary;?>
                                            </tr>
                                        @endforeach

                                        <tr>
                                            <td colspan="15" align="right">Total {{$total}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    @endif

                                    @else
                                        No data to preview
                                    @endif
                                </div>
                    </div>
                    <!--/col-span-9-->
                </div>
            </div>

            <script type="text/javascript"
                    src="{{asset('bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
            <script>
                $(".choseMode label input").click(function () {
                    var attr = $(this).attr('id');
                    if (attr == 'text') {
                        $("#fileCsv").addClass('hidden');
                        $("#input").removeClass('hidden');

                    } else if (attr == 'file') {
                        $("#input").addClass('hidden');
                        $("#fileCsv").removeClass('hidden');
                    }
                });
                $("#form").validate({
                    rules: {
                        basic_salary: {
                            required: true
                        },
                        month: {
                            required: true
                        },
                        gasoline: {
                            required: true
                        }
                    }
                });
            </script>
            @if($errors->first('file'))
                <script type="text/javascript">
                    $("#input").addClass('hidden');
                    $("#fileCsv").removeClass('hidden');
                </script>
            @endif
            @if($errors->first('error'))
                <script type="text/javascript">
                    $("#input").addClass('hidden');
                    $("#fileCsv").removeClass('hidden');
                </script>
            @endif
            <style type="text/css">
                label.error {
                    color: #F44336;
                }
            </style>
@stop