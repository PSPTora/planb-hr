<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	@yield('style')
	<link rel="stylesheet" href="{{asset("css/bootstrap.min.css")}}">
	<link rel="stylesheet" href="{{asset("css/select2.css")}}">
	<link rel="stylesheet" href="{{asset("css/styles.css")}}">
	<link rel="stylesheet" href="{{asset("css/table.css")}}">
	<link rel="stylesheet" href="{{asset("css/toastr.min.css")}}">
	<script type="text/javascript" src="{{asset('js/jquery-1.10.2.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/toastr.min.js')}}"></script>
	@yield('script')
	
</head>
<body @yield('class')>
	@yield('content')
	@yield('script_footer')
</body>
</html>