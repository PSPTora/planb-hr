-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 14, 2019 at 09:41 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `planb_hr`
--
CREATE DATABASE IF NOT EXISTS `planb_hr` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `planb_hr`;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2017_04_26_062010_create_roles_table', 1),
(3, '2017_04_26_062030_create_salaries_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Administrator provides both clerical and administrative support to professionals, either as part of a team or individually', '2017-04-27 00:03:07', '2017-05-01 20:33:20'),
(2, 'User', 'When you add users to a site on Tableau Server, you must apply a site role to them. The site role determines which users or groups can publish', '2017-04-27 00:03:07', '2017-05-02 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `salaries`
--

CREATE TABLE `salaries` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `month` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `basic` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minus` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_day` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basic_salary` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gasoline` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attendance` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra_plus` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary_tax` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(11) NOT NULL DEFAULT '2',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pob` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `register_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `role_id`, `email`, `password`, `phone`, `sex`, `dob`, `pob`, `address`, `picture`, `status`, `register_date`, `key`, `remember_token`, `created_at`, `updated_at`) VALUES
(527, 'Chhun Chamrong', 1, 'chhun.planb@gmail.com', '$2y$10$adLmAqWoZAcY/AE0gh2ajOPLHHyuBcD06F0KTt6j1Ub/LAtf6VyLK', '012 800 482', 'Male', NULL, NULL, NULL, '', 1, '2017/08/11', NULL, 'HHMkALPaZxve3CYgsrgCJjtBC6YPiG5Cw7EFXGIQZusSsFESTjDG1IDMsErH', '2017-08-10 18:39:22', '2017-08-10 19:42:49'),
(528, 'AM SAMOL', 2, 'am.samol.planb@gmail.com', '$2y$10$VZFKxbbpoAMUrCQ3X8UFi.hskcf6D.JeW5Z0SGZdnur5BxoOD6I6a', '081 420 008', 'Male', '1990-06-02', 'Kampong Speu', 'Street Veng Sreng Songkat Chom Chao Phnom Penh Cambodia', '1502422115photo.jpg', 1, NULL, 'cea5ea533c0c800d824dfc529d59f222', 'iKbPfiltTAKRBEXiAYcv5uR5cTR5ZxQrrp7KS42Juh2ax35wN35Mu5FEn7ZK', '2017-08-10 19:55:06', '2018-09-09 20:20:17'),
(529, 'CHANN VUTHY', 2, 'channvuthy.planb@gmail.com', '$2y$10$ovzcvbxIP4hCRJtd.1FgqeHc9XZqCSd3r.xHGdekxk2vpG7X0qwMe', '069 31 53 71', 'Male', '1991-02-05', 'Takeo Province', 'Sangkat Tek Thla, Khan Sen Sok,Phnom Penh', NULL, 1, NULL, 'fd55bd292e41daaf1add16ec78e80066', '2RrRqzmFPo4vJWSDcBoQv9mjeOFeBKDRqXO0aVHfSiAspQF9QomEFi9Gpwq9', '2017-08-10 19:55:06', '2018-10-30 21:11:07'),
(530, 'CHHUON DALAY', 2, 'chhuondalay.planb@gmail.com', '$2y$10$4a43jP5kU5zPr.Al0ordK.vVFbaSkJwtxTrxFBlSrA43KzvL8WwxO', '069 60 35 75', 'Male', '1994-02-16', 'Takeo', 'choam chao', '150242198420562936_1937308719846442_1216906823_n.jpg', 1, NULL, NULL, 'xVvOjX5WsumTvPCUWfMKEnxRQSES5cWLoqifHeATpSoWSTdOUtbdKseeALuK', '2017-08-10 19:55:06', '2017-08-10 20:26:24'),
(531, 'HAK THENGSEAP', 2, 'hak.thengseap.planb@gmail.com', '$2y$10$gKoEcGKofHOHiU2UTW.AbeuotgNaBaimWXzGcOY6ocu66qzwtBEsC', '070 51 29 23', 'Male', '1991-08-15', 'Kandal Province', 'Phnom Penh', '150242595014915665_859183234219299_2180836398368011798_n.jpg', 0, NULL, '3e4c254a826789be042972a38d44e561', 'OVLl54O1Nb8uTOL9I7YHq40LeBLsjSKHpxwS15xJoe7gM3ZZTaLImzIo3DgE', '2017-08-10 19:55:06', '2018-08-06 03:30:35'),
(532, 'KHEAN KOSAL', 2, 'kosal.planb@gmail.com', '$2y$10$nn17OuNO0e21pK7gxLZsJubVGHlBdfbYJF9Hy51FLF8U.WPY9zD76', '097 37 89 463', 'Male', '1992-03-15', 'Kratie', 'Street 21A  Svay Rolum, Kandal', '1502422576C360_2016-01-16-08-23-35-924.jpg', 1, NULL, '63758ad764ab999aac1a0b60031d13c6', 'a4U0ibKEFtY7B5seICShDeFiE6sYkdF5p2fgdfLJuI9D3hteK0Ew6rwycBxk', '2017-08-10 19:55:06', '2017-08-10 20:36:16'),
(534, 'ORN MOEUN', 2, 'ornmoeun.planb@gmail.com', '$2y$10$iXQMjltuS2B25Vc4kxMmguq46n7WkZtxRKG1ts8zYuQj12ddc5VNa', '097 63 48 809/098 43 90 66', 'Male', '1993-05-20', 'Kampong​ Trach,kamport', 'Phnom Penh', '150242542820246392_765794033600412_3872570163870551552_n.jpg', 1, NULL, '8990050a51edb7afe3acb381b6e53a97', 'wylOir7uBK7BYnJqn71W9oZO4X6gopgnOr35P6RppqHuqXCFlAS52RYjZNfL', '2017-08-10 19:55:06', '2018-08-30 03:07:06'),
(535, 'UY BACH', 2, 'uybach.planb@gmail.com', '$2y$10$Tedk9puo9NQUdmVa8x2XSOG9RRjmBBFMCKTtkjm0MTbbKgLO3W.Ei', '070 38 39 21', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'YIyUzSGDTLK232K71nZDGtmsHFgL6uOZKSBGdYwIL7lydtqoH5gYPlFMzDj1', '2017-08-10 19:55:07', '2017-08-10 21:16:40'),
(536, 'VON PHEAROM', 2, 'phearom.planb@gmail.com', '$2y$10$aux.vv4HfaUYdgSK8aaDjuMTrnXmavpJdm5C5//muSGAa/jfLrm4i', '086 567 547', 'Male', NULL, NULL, NULL, '1530678667IMG_4503.JPG', 1, NULL, NULL, 'JZH2Rwmuc5e4Kbz3UUwkIJJFveF0Xuev7OBwoPajCPa96TgVbnV4AuCvmPnn', '2017-08-10 19:55:07', '2018-07-03 21:31:08'),
(538, 'CHHON VANNA', 2, 'chhonvanna.planb@gmail.com', '$2y$10$oaHP9LteOUO5YEFeqWyqk.IoZN9oM3Qdy9KKTcRJwXep.2X8/wpSa', '010 591 026', 'Male', '1990-10-25', NULL, NULL, '1502425400vanna.jpg', 1, NULL, '90b876a4aeb5246c9b24c378829e9a8c', 'ld14cNbIqk5lcAnjYxih1gYCJkRueFKeySnVJabnvWmz05AS422lvOazXld9', '2017-08-10 19:55:07', '2017-09-12 23:10:21'),
(539, 'HIM KHY', 2, 'himkhy.planb@gmail.com', '$2y$10$kSkb27zsGsKBl//4BTIGQeFLWxVXGkLgDsRSdauTTS4gkJkDMwO1O', '088 84 88 880/010 53 80 66', 'Male', '1992-07-09', 'SamboVillage, Sambo Commune, Ba Theay District, Khompong Cham Province', '#208, Sangkat Toul Sangke, Khan Ressey keo, Phnom Penh, Cambodia', '1502425767photo.png', 1, NULL, 'f29de078649d5dcc1ba12a84d1757670', 'bC6yfpNlx08QQSnSX9zVTGS1UF0LGQGWHTjJAOZScLVA2tIdz2SmS2nn6Cr5', '2017-08-10 19:55:07', '2018-08-20 01:17:43'),
(540, 'KONG SOVAN KIRY', 2, 'kongsovankiry.PlanB@gmail.com', '$2y$10$UptuhvzyxhS/uij3Vg4ddeJAKM.idnlB4LwXOATkjzaZOpLTiUOKi', '096 20 00 408', 'Male', '1993-04-08', 'phnom penh', 'camkocity', '1502421631512x512bb.jpg', 1, NULL, NULL, '8m5pdLQjGe54uJFKMv7LHkFSpkXB928nLDhkdHYtn9DWYi4BWwzg8qITbDqS', '2017-08-10 19:55:07', '2017-08-10 20:20:31'),
(541, 'RUN YEN', 2, 'runyen.planb@gmail.com', '$2y$10$UQKKRUKBN5Aev7sv3ei1pudtHdtzOsQI5Zth1nMXBWJ7ZWR8eK6/a', '096 4568214', 'Male', '1993-04-02', 'Prey Veng', 'phnom penh Tmey, Sen sok, Phnom Penh', '1502422178634250.rsz.jpg', 1, NULL, NULL, NULL, '2017-08-10 19:55:07', '2017-08-10 20:29:38'),
(542, 'SENG KIMYIN', 2, 'sengkimyin.planb@gmail.com', '$2y$10$fwng3KbSV9y1PPEusBQrCO4cSmkOBveqxY0BGxumVHUu1bwIZJ4tG', '096 24 90 991', 'Male', '1993-06-07', NULL, NULL, NULL, 1, NULL, NULL, 'j8lAsFSznjhRN24z55czoEMVzsMZcBlq8tJkn1BvMQgbmnkde4S7UTnc5Cgh', '2017-08-10 19:55:07', '2017-08-10 20:31:25'),
(543, 'SNGOUN CHANTREA', 2, 'chantrea.planb@gmail.com', '$2y$10$5k3BCw2yzSZFojMxTECUzO0gaEusRvjAMOiHiP.z1/YskQCuhmeNq', '017 90 00 30', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'GjKFNSjQyFuKApUKPrtGOccEhyj0SQ0hTxyOeWvXJHj9txo2iqsCa9xStPby', '2017-08-10 19:55:08', '2017-08-10 21:14:07'),
(544, 'SOKHOM CHANNA', 2, 'sokhomchanna.planb@gmail.com', '$2y$10$CLqnUWtBEnnU7NUD4V3tU.2aya9XHIN4WdtSo0lMMsb/7CVRNmLVS', '093 50 40 99', 'Male', '1992-09-03', NULL, NULL, NULL, 1, '2017/08/11', NULL, 'QplXdAP66m9sOuvlxpPN68UJMlrtIJM7DEeA3MKe0ZgEVcYOtdMDbD6vukXq', '2017-08-10 19:55:08', '2017-08-10 21:49:04'),
(545, 'TRY SREYNETH', 2, 'sreynethtry.planb@gmail.com', '$2y$10$mV9HAWJU/KCZU08avY5NBug5PqARRdQ/jbsQB.zvPj0v7c6SxxEsW', '096 20 68 258', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '6Zei2qDVf4DeR5NXrHKfZzt6GQoRI9TpSReq4tBWRVCZwxdrVQeIJT8diOKQ', '2017-08-10 19:55:08', '2017-08-10 21:20:39'),
(546, 'YONG BORA', 2, 'borayong.planb@gmail.com', '$2y$10$vwrzMr1.q0rk6CfXSOcRL.gu.gqAYOVg7AVOtW0gtDXc6dWRGSBpK', '096 70 04 570', 'Male', '1991-07-14', 'Banteay Meanchey', 'Phnom Penh', '1502422266Bora 562.JPG', 1, NULL, '0cc4972406457c0c144b5863d4e139d8', 'HejRsREk7ONHDouLMfY2IlwH3VJ3o6bF4MaiwhlESRfWK5YcmuGNe2v16wDo', '2017-08-10 19:55:08', '2018-09-10 00:14:07'),
(547, 'TEAV PISETH', 2, 'teavpiseth.planb@gmail.com', '$2y$10$bQmOdWmwNTS/fqkV49W59eezysaopaYIkKzW.s6r.sGU5Y4YSEKdS', '098 69 29 88', 'Male', '1993-09-22', 'Tulsongkae village songkat Tulsongkae khan reseykeo Phnom penh', '51H Tulsongkae village songkat Tulsongkae khan reseykeo Phnom penh', NULL, 1, '2018/12/03', 'cd6274314204f0b7342f35ae9dfa0165', 'T0Buam43iqx0PhibUZ1smWa2tz6Vrvs0umSnw8MfbicaZtxxNgsxMC1jd3Oy', '2017-08-10 19:55:08', '2018-12-06 23:18:27'),
(548, 'SAN SOEU', 2, 'soeu.planb@gmail.com', '$2y$10$PDr1TgytLYrzoP0kJvILV.KHbPIX1Q7QnjYg4GRHMhvA4qvzJpYt2', '098​​ 385 568', 'Male', '1990-05-15', 'Kompong Cham', 'soeu.planb@gmail.com', '1502421865DTV_Brj-.jpg', 1, NULL, '38cbe7e6874c5c6ee9a55807dc42c9ad', 'THfLbDvxnJxC6pGlgGPSWdZRpu5d9yGXPvEPXBcGbef85EeXdn2aGb7Doia4', '2017-08-10 19:55:08', '2018-06-18 22:04:20'),
(549, 'SORN NAROMREASEY', 2, 'reasey.planb@gmail.com', '$2y$10$5WSriphpw5kCz2I7r5vCaOHgBDqN7Qhdt/w9wO0EBhyIesBU6HSqy', '087 71 66 27', NULL, '1993-08-15', NULL, NULL, NULL, 1, NULL, NULL, 'q3KS5FR8XA73NpN7XHqir9ZlLwveNx3ezNAK1Jxpc9W0ZzJ6mMJwCO9SimGj', '2017-08-10 19:55:08', '2017-08-10 20:22:22'),
(550, 'TAO CHANTHY', 2, 'chanthy.planb@gmail.com', '$2y$10$8EVZ4VnPMlj0/bINEIvoj.aboCtCsAZKiwbb1oI1G1gDQ8QohoTxW', '098 75 60 36', 'Female', '1992-04-12', 'Phnom Penh', 'Phnom Penh', NULL, 1, NULL, '680db36c93107f0f445c4472bdc28422', 'YeGKJX99G1DdTQICUIMSU5WCUfakpeKhyRL0Ml9F7JLyG0rpFl3agYAVCydW', '2017-08-10 19:55:08', '2018-08-14 03:24:17'),
(551, 'SENG MUYTY', 2, 'muyty.planb@gmail.com', '$2y$10$K2GqGcZf12HQEwrbgplbK.CKpf/QtCTsP7fuFkXM5RtShs.MsUAYS', '077 66 83 38', 'Male', '1994-11-27', 'Kandal Province', 'Phnom Penh', NULL, 1, NULL, NULL, '69GZyM360iSdK1Bgf7Y6zfGbD1njy36fuBqOhBE4ETWYlQG8Id2EcLOLvRgS', '2017-08-10 19:55:09', '2017-08-10 21:22:43'),
(552, 'SROR SREU', 2, 'srorsreu.planb@gmail.com', '$2y$10$1qSUiT1hxD36MAwJNvfGv.jkX7Kepe07PUtYMntG//.eUVYnCnhj2', '010 64 78 59 / 097 222 86 56', 'Male', '1988-05-07', 'Takeo', 'phnom penh', '150242397217523424_1138463569610321_1468127404181304098_n.jpg', 1, NULL, NULL, NULL, '2017-08-10 19:55:09', '2017-08-10 20:59:32'),
(553, 'ROEURNG CHANMONY', 1, 'roeurngchanmony.planb@gmail.com', '$2y$10$2FU2muVdL/VqqTlf4qdaTuxbtNGeW6zMHYZZJ1FmR85dMZ7k7cFaK', '015 52 65 15', 'Female', '1993-08-16', NULL, NULL, NULL, 1, '2018/09/09', '25c9353d4f167a65c8934e24e9ef3e2b', 'JmjmZvPyCE18dNisKqBaLo6QJwwWGmh0hRY36ZiKcqNALku5MFR9qlZHuM0j', '2017-08-10 19:55:09', '2018-09-19 02:20:39'),
(554, 'CHAO CHHEAN CHHEAN', 2, 'chhean.planb@gmail.com', '$2y$10$1vg4Lhi2txaqDNd8OczHWuxyzWt4G9IbievmxPUj8mbrtS.togJDS', '010 46 26 51', NULL, '1996-07-31', NULL, NULL, NULL, 1, NULL, 'c9619dab4abaebd19bf36a194ad5f77a', 'sQ7TbF2kW72QrojxFdSs5feQbtHlGDSqVAUYlgea7kveUNXyskcVjxJQIBuF', '2017-08-10 19:55:09', '2018-06-11 22:58:50'),
(555, 'HEUMBEG TONGHENG', 2, 'hengher.planb@gmail.com', '$2y$10$oaq.a3zXaS7k1UTnu2pQ8.2dF.wnI7bus0hdn7XsBNroVhQefqEzW', '010 97 92 40', 'Male', '1994-01-03', NULL, NULL, NULL, 1, NULL, '939f6c9dc3ca214dac6900b277c79d5e', 'K0z8mJbtWXhU2Glhk8VZY1SEPg7XInmcU9lUjHFipsa1nn6qrsJ4UucFGs5N', '2017-08-10 19:55:09', '2018-09-10 18:38:31'),
(556, 'DEAP SINOU', 2, 'deapsinou.planb@gmail.com', '$2y$10$HgOzWcoSAENJTrSmcsz2ZuJ.jgpxClCdl0NdVwCUoeOtPHZ.G70Yy', '070 36 80 72', 'Male', '1994-09-10', NULL, NULL, NULL, 0, NULL, NULL, NULL, '2017-08-10 19:55:09', '2018-08-06 03:31:04'),
(557, 'HEAN LAKHENA', 2, 'lakhena.planb@gmail.com', '$2y$10$kaOGFsTmffb8kSD2q6Dmt.Ta36xQMZp5TLTjF1NYzztCA5ieK3ZPm', '098 713 790', NULL, NULL, NULL, NULL, NULL, 1, '2018/04/10', NULL, 'j5QSagtJSrNRu5kwmUDSV412pzQkhzgXBT15c3wKkXCyJRVH5RYDJBVn9YvM', '2017-08-10 19:55:09', '2018-04-10 02:19:34'),
(558, 'MON BUNTHA', 2, 'mbuntha.planb@gmail.com', '$2y$10$gvet5/zUeXetqNDlTFqZmets3uG7jvxDgWtM87FvKbg7EJKSVlS2a', '092 800084 / 010 921 314', 'Male', '1981-03-18', 'Pursat Province', '#27y, St. 565, Sangkart Bueng Salang Khan Toul Kork, Phnom Penh', NULL, 1, NULL, '41b0e80184458aa241c2ad2a5103bae6', 'AWevITADt4hBKavpb63QxO5NhpRp4LJRln0fHRX22TIDIiAxju1ToXQAsPF7', '2017-08-10 19:55:09', '2018-09-10 00:11:15'),
(559, 'PHOU LIN', 2, 'phoulin.planb@gmail.com', '$2y$10$O/XNe6781cOOUO0zcSKn0ORR1xvTnD5fsMOpg1OQSt8yjh6lGeScW', '089918188', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'd022506c9e15f4e2fb93befa24172ab1', 'x6ZuQ1AK5DkIX9Xsb6nCEAlOPuZ2L32aqlwa7kmqeXCYwntuLqHwWQ32XPIr', '2017-08-10 19:55:10', '2018-04-10 01:23:41'),
(560, 'YAN MANAV', 2, 'manavyan@gmail.com', '$2y$10$agGe80WEH1C0x8f9423.Q.mmg2kIYrX7o5JTgdOKUJI.yfHehGfS2', '0965368148', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'MiDx4OlvMYo2suTaOiaEdrlVd4zmgDm02aQG3f8mYK9UadLqrx0cSVSBFPfI', '2017-08-10 19:55:10', '2018-08-06 03:30:08'),
(561, 'HENG CHIVA', 2, 'hengchiva.planb@gmail.com', '$2y$10$TgyQjIjBDf3le2gq8S9o4OAQ2UOEengWWGYfz2OMZK.p1NYh4gwHK', '096 35 99 659', 'Male', '1994-05-20', 'Takeo province', 'Phnom Phenh', NULL, 1, NULL, 'a125cf6e8b86d6f4db28969f0fc41496', '41xvSCNKGNt8YW67w3HIgfdJ3dz6X3AxZJ4Ck1olNwxhAjB8rFUBdkQBjBhA', '2017-08-10 19:55:10', '2018-12-06 23:50:15'),
(562, 'YANG SREYSAL', 2, 'sreysorl.planb@gmail.com', '$2y$10$YJBmkMJFRVRg1JNyx8rosePdjNQs0msMSm28ETA/k1brONO6BKZie', '010 56 86 89', 'Female', '1995-07-02', 'Kompong thom', 'Phnom Penh', NULL, 1, NULL, '8aa5ede5c7e69a7afb1e121a0fac6424', '8PpEEPU4KVduNXasrgLxXWJHgnhjqftUOjo21dARNEsgG6Yr1MEWkT9q0RCj', '2017-08-10 19:55:10', '2018-12-06 23:08:09'),
(563, 'SAMBAT SOVANRAKSMEY', 2, 'sovanraksmey.planb@gmail.com', '$2y$10$aeREAH9fKvSNxzS3l12Wqef9Se7PgeJFNnSeh0zRtTS4CsX/ABShm', '096 28 72 975', 'Male', NULL, NULL, NULL, '1502424936IMG_2256.JPG', 0, NULL, 'dff7cce9341192f99f00a4af48eb33ea', 'cXWEakkvAFj8dTqzwiu63LRbUa9X4beW4yvRHImKCLoqv4ZY6BjAjlUagQgw', '2017-08-10 19:55:10', '2018-08-06 03:30:28'),
(564, 'SEUN HAK', 2, 'hak.planb@gmail.com', '$2y$10$wSl3ynYKjIocIaAoXEmUWOB6bnGe3RP1JV9d6umYtHBwmGKp7e32.', '010​ 41 16 59', 'Male', '1993-11-11', 'Kampot', 'Phnom Penh', '1502421829SEUN-HAK-(1).jpg', 1, NULL, NULL, 'hMx8KAnQ6nTshKYgdxUGA2bthBQsWZPhKQOIJtd4nH2qLpJw54MfjfHdRbKj', '2017-08-10 19:55:10', '2017-08-10 20:23:49'),
(565, 'POR PISETH', 2, 'porpiseth.planb@gmail.com', '$2y$10$jShBOkiP3owOU3rw9xd8PurUElvk3srM3I3w3rYp0OovQCL0p0ctK', '096 40 94 760', 'Male', '1996-04-18', 'perk village, krek commune, punheakrek district,kompong cham province', 'phnom penh', '150242284015036394_1742610356002177_5703241792399934476_n.png', 1, NULL, NULL, 'BuDKnm4CMVuHhWYLPG3Wf1j9k1Bn6pE2dyU8dbfGsFqsTiaL0pEFTdANwnUb', '2017-08-10 19:55:10', '2017-08-10 20:40:40'),
(566, 'KHVAY VISARASSY', 2, 'visarassy.planb@gmail.com', '$2y$10$/vhNWkMV/Y8ORrrhHA4JneL0tFaVIOWBTvtIv.ZLZfxrPujCxhRAm', '086 93 04 11', 'Female', '1993-11-09', 'Battambong', 'Phnom Phenh', '150242572317022507_1205063646276776_2886399576294076878_n.jpg', 1, '2017/08/14', NULL, 'CX84FiMNEnblWSyY1MDfzdi2eFlmyl8S2PuIh9nOqHTbtyT6LDl5PpanqTDd', '2017-08-10 19:55:10', '2017-10-09 21:35:15'),
(567, 'THA RATANAK', 2, 'rathanak.planb@gmail.com', '$2y$10$vrdPDpPhWiIyukiluQq1S.Vx8QG58J6wFJBAy4ST4UxZT6f2aBx7.', '096 26 11 180', 'Male', '1996-01-27', NULL, NULL, '1502425379IMG_4336.JPG', 0, NULL, '6dba620e7c4ce0c4d66192fd48eb9ff0', 'v7g07Ras6JJQp71iAB43m5mooFHdZ0g1EOQqBL3eb4IXpV5G7caxr0y9kktv', '2017-08-10 19:55:11', '2018-12-06 21:20:42'),
(568, 'YUNG DAYANICH', 2, 'yongdayanech.planb@gmail.com', '$2y$10$nDC28HfaMDR6dDM8TWHKz.nvlfh16ZLphPpBdiQh2GWN8Y9Wzo39O', '069​​ 22 75 52', 'Female', NULL, NULL, NULL, '1502421799pbimage0000108571 (1).jpg', 1, NULL, '475b1aac2fe7ad8aa5e4f7a8504b4c7f', 'XFHQBKkBSPiNjUc02aY3d0sEgDinIGJmEXfRfoMH6tihJBCuRQb6JloX0ld7', '2017-08-10 19:55:11', '2018-02-28 22:00:06'),
(570, 'LUON SOPHEACHAIRYA', 2, 'ounyaspygril.planb@gmail.com', '$2y$10$kSu1kVQ7lZJs/Wo.KqNkyOeYburW1USDNaPoKlDS8d5j541S6ZC8m', '093​​ 97 ​80 ​47', 'Female', '1996-08-18', 'Prey Toteng village Jreai Veng  Commune Prey Chhor District Kom Pong province', 'Tek Tklar', '1502426266maxresdefault.jpg', 0, NULL, NULL, 'VZlwN9uSzaxLw4PY8Y9xcOEQsLvnoTGgaV9hN9kFqWqsFXDg9kdWcJoPGUEL', '2017-08-10 19:55:11', '2018-08-06 03:31:53'),
(572, 'DORN SOTHEA', 2, 'dornsothea.planb@gmail.com', '$2y$10$q9vHZ.vthVBhrGB4ZtrfAO/kx.B81paryOOgbPulY57ZTx6a42mf2', '069 56 80 29', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '97PsguNbvfaaRYi1i6nqkCISOgy5WBY14AuqPI3D1vpiugngXcDqKC6nfJB7', '2017-08-10 19:55:11', '2017-08-10 21:32:11'),
(573, 'PHON MARY', 2, 'phonmary.planb@gmail.com', '$2y$10$2QIA/ppfdUIzPE2kvPxwmuktVYX.zijziGX7qtrMhzwKk3bQgIYN.', '096 23 788 23', 'Female', '1995-03-08', 'Takeo', 'Takhmau city', NULL, 1, NULL, '7372e8709025c607fe9eea90a8af051e', '8Z7fhSAuNpGdwOw5FztqQtBaDWVW3LUaHicLCu71kR8l0IuqrS7kpYIM6OhE', '2017-08-10 19:55:11', '2017-08-14 21:25:21'),
(574, 'CHUM CHAKRIYA', 2, 'chakriya.planb@gmail.com', '$2y$10$UoR6PGSRnyDByOXql2CFvuYrSuOmxiPxMeoO.XLabizMdsPj3p3Ka', '093 649 343', 'Female', NULL, NULL, NULL, NULL, 1, NULL, 'fe7fb5b07e72161a27cca446df46743e', 'IPcPY59naSXEMnGJqZ1xM6V3C1ayM4k0CuZP7C9BwK3WgCPzkG2E4QMIKvvw', '2017-08-10 19:55:11', '2017-09-12 23:09:22'),
(575, 'SORN ENGTRAY', 2, 'sorneingtray.planb@gmail.com', '$2y$10$ud6xe7krU2cDnHirUBI64uu4t0AKEGdnD65rEh5VA96lLzwhMNxka', '098 22 11 92', 'Male', '1997-03-31', 'Takeo', NULL, NULL, 1, '2017/09/13', 'f8e083910fe9ae7485ee2feb48ab8f15', 'zTGy65htjLc3hBzYFyOopLKLsy7sJfFGPKL43LVWY4V0OcbVLCO19oUuMfTm', '2017-08-10 19:55:11', '2018-02-28 20:18:18'),
(576, 'TOUCH SOKLEAP', 2, 'sokleap.planb@gmail.com', '$2y$10$2uksnpJoBnDysYatFiiSWuIIlHnRlQKSIfnWWF2LZHwCVx2b4fdk6', '096 91 56 157', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'd42afded825dcfdab2b840f80fb9bf66', 'iaVIWeAoblp7vZLl5CBD4Y3hRppXl79reojELrXfQQ60HDaidskoDiFcvEjI', '2017-08-10 19:55:12', '2018-12-06 21:21:00'),
(577, 'THEA VANNAK', 2, 'theavannak.planb@gmail.com', '$2y$10$0UO2vykROo8yhTCo2CgldO5PNUIf67GDkCe2KJOwU.0.ZvwIwpyZW', '096​ 93​​​ 13​ 600', 'Female', '1996-05-03', NULL, NULL, '15024326081.jpg', 1, NULL, NULL, '5Od2sPyFDP66bJ8GMQCjXgVCuZ9P0s5Jj48AA26EM6BU3fNVGJtMiCmT8D9o', '2017-08-10 19:55:12', '2017-08-10 23:23:28'),
(578, 'TY VIROTH', 2, 'tyviroth.planb@gmail.com', '$2y$10$JDQO1fCrPA0Z8L1tyD2r0.OfeVUMwNnm11ds7oRPNHKVuBwLcqM/u', '092 883437 / 093 262393', 'Male', NULL, 'Preah Vihear', '118BE0, teok laork 3, Toul kouk', '152844799726770939.jpg', 1, NULL, '78dc7bb36c3b6d73a5a847c706bb7825', 'GStu5ueCkRuSIaD08c0IDjgpAadu7yZCS7wb6Lv7J6E8o4r1IlwH8mQ9wdYF', '2017-08-10 19:55:12', '2018-10-04 20:45:36'),
(583, 'LUN SIM', 2, 'lunsim.planb@gmail.com', '$2y$10$5VY/mXskg17yZh4QkACE0.lcormaqmbOWmOnejCKODk9cA2p20YQW', '096 5969446', NULL, '1996-03-15', 'Kompong chnang', NULL, '1502432955Penguins.jpg', 1, NULL, '3b1f14504edb3d666d81e2081c1f6dc2', NULL, '2017-08-10 19:55:12', '2018-05-10 22:34:05'),
(584, 'VUTH SOPHEAKTRA', 2, 'Sopheaktra.Planb@gmail.com', '$2y$10$9f4Q0R98/sLy5OnscNTW4evYmiIYlGu8vyeo6B./YksJSoic2l9j.', '086 292001', 'Male', '1996-12-13', NULL, NULL, NULL, 1, NULL, 'd955090edb8756251eadc27a5daf891a', 'Bi23NIAbkAOIWmOIAAJA8dpckRZiRnOIa13979wB64RMkjYgiHpJpPkqoqAt', '2017-08-10 19:55:13', '2018-12-06 23:06:24'),
(588, 'PANHA SAKVISA', 2, 'panhasakvisa.planb@gmail.com', '$2y$10$EQw2MZ0vltG0ixcyRnuq4O18CEEkEiVHjTNcvIuUPkoqhZCGyJKeS', '096 76 72 517', NULL, '24/03/2017', 'Phnom Penh', 'Phnom Penh', NULL, 1, NULL, NULL, NULL, '2017-08-10 19:55:13', '2017-08-10 21:16:01'),
(589, 'TANN MINEA', 2, 'minea.planb@gmail.com', '$2y$10$r6Ex5LbDUQQEU1FIlFT3.eh.q5sYneHHhMazKi8LiKEexmrQA8lXq', '096 4074573', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'M9jfjaEi9yzDxm9ZbadFb5UsDnghwnmN7LI57PKmULu1V1mKRJ6Cc9yrL2BZ', '2017-08-10 19:55:13', '2018-12-06 21:20:47'),
(590, 'KRY LINDA', 2, 'lindakry.planb@gmail.com', '$2y$10$.g480dQGvH4ev.XJOcMAu.geIjZZEzs6GZ5zhFkC.WUPAWenEu5tS', '098 26 24 29', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'rGFZiyVgieSlcUNYgSxEXmkNHIHhRfcsDoPjs33zGUKeG5iWFnzD2wh61UlK', '2017-08-10 19:55:13', '2018-08-06 03:32:19'),
(592, 'UTNGY PISAL', 2, 'utngypisal.planb@gmail.com', '$2y$10$g8LSkk6bwl1/PpfhwRy7zuXQ..bkFl2uqxfOCa1FJHFytvum7iX66', '0885661110', 'Male', '1998-02-05', 'Takeo Province', '#116 E3, St. 186z, Tuol Kork, Phnom Penh', '1502421623myprofile.png', 0, NULL, '7da5bc4daadb3f9012e7d1144a303fb6', NULL, '2017-08-10 19:55:14', '2018-12-06 21:19:21'),
(593, 'HEOUN PHIRUN', 2, 'phirunkonaka.planb@gmail.com', '$2y$10$VcqRr9dU7ktYNT1KsZp/9OU7L.hHWHWwXY1luz2rfO5St.lxgWzgS', '0962601235', 'Male', '1995-09-01', NULL, NULL, NULL, 0, NULL, NULL, NULL, '2017-08-10 19:55:14', '2018-12-06 21:19:32'),
(595, 'PHAN RAKSMEY', 2, 'phanraksmey.planb@gmail.com', '$2y$10$.obzgoOEeoGGhv8I8aUlrONfT8FpZmLdbgHU43z/ZUhOs9e.C6g6m', '081477438', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'uqdnBBtYzcgE982umi2M7NKZJMjgMo9qHqGpIRsxqQKBy0mVESWbCo9Y7X1h', '2017-08-10 19:55:14', '2017-08-10 21:32:22'),
(597, 'LENG RANY', 2, 'lengrany09@gmail.com', '$2y$10$b5VATt/6v17eDQp104N0sOuWtPxoGfwMsAs2W5wRm8P/dMFYKVXoO', '069 68 98 86', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2017-08-10 19:55:14', '2017-08-10 19:55:14'),
(598, 'PHIM SOPHEAKTRA', 2, 'pspt.planb@gmail.com', '$2y$10$PUiTTRE.JX5M2Qwm5Fln7eWrXJ4U/MUB4ykD0vZZHgizbfvfyOaXu', '010 223 536', 'Male', '1996-10-11', 'Phnom Penh,Cambodia', '#216, St 37BT, Beong Tom Pon, Mean Chey, Phnom Penh, Cambodia', NULL, 1, NULL, '084c141bcb1a60bb9619aafc340241fd', 'wzQfj0plRgsws5SzC3vujbgW7dfeihy7uFFYCgAJvurdwpW4dpGUqzxY8kar', '2017-08-10 19:55:14', '2018-10-30 21:11:46'),
(600, 'CHENG CHANNA', 2, 'chengchanna.planb@gmail.com', '$2y$10$p43OWxe5BYDxxr3jAQKaZeAP5NbJ7ttV1KV9G7A3i5701tzZ4PBFy', '0965929291', 'Male', '1997-02-25', NULL, NULL, NULL, 1, NULL, NULL, 'esHF7ZDOir8JHXIZbbcdPEjJvGfbfSniO0wNxEoZmqw6QVc1sR2IM8kx2kLq', '2017-08-10 19:55:15', '2017-08-10 23:13:43'),
(601, 'HOK KOSAL', 2, 'hokkosal.planb@gmail.com', '$2y$10$VwFnLAQZOnIkNe11CzPIouUQQ9cu/lWVmWj1T.JxVSFCumMOMRYGW', '016503584', 'Male', NULL, NULL, NULL, NULL, 1, NULL, NULL, 'xNtfmWE3qBKigP2udG45WQLa3hwP06qRUcLKw7Q8IbwaeyZmdrGj7vZf9M1S', '2017-08-10 19:55:15', '2017-08-10 23:12:05'),
(602, 'SY TECH HONG', 2, 'stevie.planb@gmail.com', '$2y$10$4PISj7loUhabjeZldQIcB.ebxeY6uEVB5l79M9oYzHAKSErcyOe3G', '010 678 654', 'Male', NULL, NULL, NULL, NULL, 0, NULL, NULL, 'ruvFKinHagpTKEKy8CMLmK3zv4DkhmM649AiOtX4x0MGccwr99eYYbtucTCx', '2017-08-10 19:55:15', '2018-08-06 03:32:28'),
(603, 'YOY SAMOUN', 2, 'samoum.planb@gmail.com', '$2y$10$he.ESZ8o.Hytm2lpOEqZZOfsnnnNccKDy7rHMH7v6vDzA8ee7btGS', '093 850 950', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2017-08-10 19:55:15', '2018-08-06 03:32:36'),
(604, 'IM VUTHY', 2, 'imvuthy.planb@gmail.com', '$2y$10$sfH04mnsE3oMzGDsHUyvy.CkWrKC3fCjuaH4Gojtsoap/WE9Rog0O', '096 73 50 533', NULL, '1997-03-30', 'Phnom Penh', 'Phnom Penh', NULL, 0, NULL, NULL, 'RonwPljERKbLJsHX9amz5BF6cymy74KZm34B8mHlIJorV6phPsoyZqX3QZeI', '2017-08-10 19:55:15', '2018-08-06 03:32:43'),
(608, 'Kea Rithy', 2, 'rithy.planb@gmail.com', '$2y$10$wvsF1Fc9ldfxGgcYDDdpNeHPQvns8pN8vutitpn10dVFSUQPBOH2K', '096 99 15 466', 'Male', '1997-07-07', 'Kandal', NULL, '', 0, '2017/11/17', 'f5a4fd1e2e71d42aeeb8371c113bda4e', 'WSwO4rqsQTHx7KjWVcfnkugPBJhOYnOUVfDifUz8FuwOB3TJZsoltpgt259U', '2017-11-16 17:57:38', '2018-12-06 21:21:24'),
(610, 'THY LYDA', 2, 'thy.lida.planb@gmail.com', '$2y$10$57/X13Reu2FhMESLJzcXoeyng0/cf/Cp3Kha5nk7U1Be8e/qaiR/C', '015 79 59 03', 'Female', '1998-01-03', 'Takeo province', NULL, NULL, 1, NULL, NULL, 'jEYTrqxWyo4Q06rycW8ktcR45uMdI75ds3fVEKTCTdHhppIPMEn2M9ujk3LH', '2017-12-10 20:15:34', '2018-08-20 20:54:50'),
(611, 'TUON PHENG ANG', 2, 'tuon.phengang.planb@gmail.com', '$2y$10$J4DQAunyMZNCU4TPJYC/8OH7Wh2Dl.Ty7eqEjlwNQC/zjNGxyOri2', '015 92 82 78', 'Male', '1998-01-03', 'Phnom Penh', 'tuon.phengang.planb@gmail.com', NULL, 1, NULL, '33294cb0c9678340728b6ce6d02157f4', '6QucMcPK2YUpECmeaqMvZv9ACCWhdIBvgnZwtjUcHnWe5ld5D97Teeg2RLnR', '2017-12-10 20:15:34', '2018-10-06 00:57:21'),
(612, 'NHOR SENGHONG', 2, 'nhor.senghong.planb@gmail.com', '$2y$10$KRy3uABRihKNrOKe.0o/HOVcLXrsejtYc6X0nefjPSo5.XjYGOS/u', '070 99 93 31', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'd3130ec94e2becf8e892bceab53991cb', '2QLo54oChs1CboXJMMACJVuNW31bxDdL2orI4RUgnXKep8m1ZyBqJbSiFcjY', '2017-12-10 20:15:35', '2018-12-02 22:00:03'),
(613, 'CHAN BUNSAMBATH', 2, 'sambath.planb@gmail.com', '$2y$10$8u5DrrVVJAqIsxjHCuIhUuaA9qXgxHi5Rcsm45n0oFch344lxG2rS', '070 39 62 07 ', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2018-06-08 04:05:26', '2018-12-06 21:20:37'),
(614, 'KONG PANHAVUTH', 2, 'panhavuth.planb@gmail.com', '$2y$10$ONY3lpl8dLWZZBxaYcG3OeeYA2g1Osmzmzykfe/EH1qZMh6E8U81C', '069 363 766', 'Male', '1991-01-09', 'Phnom Penh', NULL, NULL, 1, NULL, '7a7bb74f2a0a42146251f5f83344e9b6', 'PQTzrjVkqcZzUWBPjD3IeuuEEbA2NsOjtLpt9LUQxraB8PJTj0mvgoycCc6g', '2018-06-08 04:05:26', '2018-09-10 03:34:49'),
(615, 'BAN VATHANA', 2, 'vathana.planb@gmail.com', '$2y$10$2ptCTYs2O7/W3OR54/rwieUeLUfLQBlOzr8wYP07toL08w3P8moAa', '089 875 493', 'Female', NULL, NULL, NULL, NULL, 1, NULL, NULL, 'As3z9qGs6Pg6AFhze7aCJvTrrEf5UKqyDHdNVnBH0rFMXYn40OGU3mqpcPtI', '2018-06-08 04:05:27', '2018-08-20 20:57:14'),
(616, 'HOT THONA', 2, 'thona.planb@gmail.com', '$2y$10$S./yEO2nwRd50bFEB4AQk.MCmpNFPHqu4EFzCDDUjhU5iZwA1tFqq', '086 674 587', 'Male', '2018-06-10', 'Suong, kompong cham', NULL, NULL, 1, NULL, '34c164d0f984c606e3498c724221177b', 'wHdjgPJYzs2UMRh33HWAALqXErzryHRjiFSzoBgvGEdgtdxq0PFV5AyiFXyu', '2018-06-08 04:05:27', '2018-12-06 23:15:38'),
(617, 'KONG VONGRATANAK', 2, 'vongratanak.planb@gmail.com', '$2y$10$qI7qSNmIK9ZSA3hYFbz/A.c8vQj3fk9ifLp9Aa4uLEK4JD.jTiV4S', '015 818 189', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2018-06-08 04:05:27', '2018-08-06 03:32:05'),
(618, 'PECH PISETH', 2, 'piseth3.planb@gmail.com', '$2y$10$DZ..9kPmRbwcC2Kj4avAIOz4GoonNwRoS8m0xyXXWtauXS6E9k41C', '096 931 3267', 'Male', '1995-04-17', 'kandal', 'st. 2004, no. 28 ,Phnom Phen', NULL, 1, NULL, NULL, NULL, '2018-06-08 04:05:27', '2018-06-10 19:25:34'),
(619, 'HOR SIEKNY', 2, 'seikny.planb@gmail.com', '$2y$10$Iq/OXq/lWtOEYENbzMeAkuK3GQTygWVfr5j1/q6xLNBbIgVsyQUHm', '081 574 087', 'Female', '1998-04-02', 'Takeo', 'Sangkat Beong Tompun, Khan MeanChey, Phnom Penh', NULL, 1, NULL, '2b6d1835a3badad8d629286c04d397dc', 'Gwdu5aA7P8NsTxKnhMBGkE1p496nqDQ2SDHI6EiIFJiQMNBLBHfIOPNqs3zs', '2018-06-08 04:05:27', '2018-10-24 21:19:46'),
(620, 'KIM LUNVUTHYRAK', 2, 'lunvuthyrak.planb@gmail.com', '$2y$10$O2M0dX7.1TEMSKkt3bdnBupyNz52TQ7fVHBGhCmtA3thW72om95ny', '096 7781 807', 'Male', '1996-04-28', 'Steong Treng Province', '#61Eo, St.301, Boeng Kork II, Toul Kork, Phnom Penh', NULL, 1, NULL, NULL, NULL, '2018-06-08 04:05:27', '2018-06-10 23:21:16'),
(621, 'BUN MONYOUDEM', 2, 'monyoudom.planb@gmail.com', '$2y$10$bZr6lw5gSxDoawOubccNm.atC5VXiZ3jhI9KlX44yZ6jrBevo4pn.', '093 775 782', 'Male', '1998-10-27', 'phnom penh', NULL, NULL, 1, '2018/07/04', NULL, 'al08pXR4XMSay6KeVvYTgw3m8JfNzch4SLCC7lXytmJx1DkugE4yN3V9R8Dp', '2018-06-08 04:05:27', '2018-07-03 19:14:37'),
(622, 'UN TOLA', 2, 'untola.planb@gmail.com', '$2y$10$.0UQKidz1/K/jkY69swO4e2em0Pnq.R02brvPE5cPLyXEmUByQeQC', '095242013', 'Male', '1996-10-19', 'Stung Treng Province', 'St. 215 #130CEZ Veal Vong, PP', '152869350532854793_158255861690171_1527501021423599616_n.jpg', 1, NULL, NULL, '0rasODuJJCKpJw5LdYB99ZTxkuIVqmpKz5PmXqlOAf84bmcqLSNEXhoPyUmm', '2018-06-08 04:05:27', '2018-10-05 02:38:44'),
(623, 'LIENG HONGKY', 2, 'lienghongky.planb@gmail.com', '$2y$10$UA7HxLoG8V80b470mVq.5uDrAplFhd7lzYq.O2nVyWHhfu45YfSme', '010 949388', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '763TQp5vtZZidDjOnkPeQ5wKTkJGMNVb6BiS2sb1RsJcRLArOFTQpxuJfBsw', '2018-07-03 18:58:56', '2018-12-06 21:19:57'),
(624, 'CZERWINSKI WITOLD EMIL', 2, 'witold.planb@gmail.com', '$2y$10$JzspBGXwIiCDRgUiGGu6feQlTbVwb9dVRKI5FdQAGqvRKnceJjuuW', '096 472 88 59', 'Male', '1987-05-22', 'Warsaw', NULL, NULL, 1, '2018/08/15', '1c7b688601da791265c48f25acd0c33b', 'xkB5zvtTcPmNMI66gh5d9l7zfjIqfjY8DiLha0PP2jtNHiob8cwFSkGYlRzJ', '2018-08-06 03:58:47', '2018-12-06 23:20:44'),
(625, 'SUON SILITA', 2, 'silita.planb@gmail.com', '$2y$10$zEDtnckOm4bKgCXObsh2AOTR9yqjPqWs/w2FBOQb79xNAR5u.aBqS', '011 252 824', 'Male', NULL, 'Kompongspue', 'PhnomPenh', NULL, 1, '2018/08/06', NULL, 'haqpoAQo3iKfYeLoxlf6RAkP07adgwiF3WYa6qjSsSeVz5C2IKq98nlSTIcF', '2018-08-06 03:58:47', '2018-08-06 20:03:40'),
(626, 'MOM CHAMROEUN', 2, 'chamroeun.planb@gmail.com', '$2y$10$ouL2JC/18QwNgezGSHGHTuBmxs7JNXxUYBjokKfIlfp53S1qnvG46', '010 41 39 92', 'Female', '1991-12-11', 'Phnom Penh', NULL, NULL, 1, '2018/08/06', NULL, 'qnCWpr58HWPWtQgSTCg4UWdFVvizisma3pqRCTOBW2AftNtgTa6AFtVoOOcQ', '2018-08-06 03:58:47', '2018-08-06 23:02:10'),
(627, 'BUTH PUTHEARY', 2, 'buthputheary.planb@gmail.com', '$2y$10$C2QsXgLOshd0sBDOBqfSJOGVBx3pxcFvGlQxNniNzyG.6RCWqNawq', '015 96 99 67', NULL, NULL, NULL, NULL, NULL, 1, '2018/08/06', NULL, 'ARYDtEmCV5V03modKek8hixlcuPMjTUqGozlCAHFgYG0pUuxHB4kikaou9uI', '2018-08-06 03:58:47', '2018-08-06 20:17:41'),
(628, 'KROCH UDOM', 2, 'krochudom.planb@gmail.com', '$2y$10$KvtWba7Rp3/USGzy./CzbO4aDvhzsn5ZvHvHOxdb/r3edWbr8KX0m', '096 53 69 654', 'Male', NULL, 'Meoungchar village, Cheatorng commune, Tramkok district, Takeo Province', 'Sangkat Toek Tla, Khann Sen Sok, Phnom Penh City, Cambodia', NULL, 1, '2018/08/06', NULL, NULL, '2018-08-06 03:58:47', '2018-08-06 23:47:39'),
(629, 'HUOT KIMPAR', 2, 'huotkimpor.planb@gmail.com', '$2y$10$20yEg.SQAui5mohnXIDBPuKB1X2wgusm33jvNbOoZ35gSizWrxTcK', '061 429456', 'Male', '1993-12-10', NULL, NULL, '', 1, '2018/09/09', NULL, 'E6mETOTPw0auPhyPyGJzCNkp9xJwTSVJV0HuZZArZbWAvp5opCnddBZ2UcTz', '2018-09-09 01:33:26', '2018-09-09 18:44:43'),
(630, 'DY YEKLEANG', 2, 'yekleang.planb@gmail.com', '$2y$10$kQD0I7tvsnIwerjKiH32ye9UQ7kuYPskVXGcBFDn52i/1Z4k3n/lK', '093 78 18 78', 'Female', NULL, NULL, NULL, '', 1, '2018/09/29', NULL, NULL, '2018-09-28 22:08:32', '2018-09-30 19:28:01'),
(631, 'HOM SRIENG', 2, 'srieng.planb@gmail.com', '$2y$10$KrQNUTJ3iiEZvBaORScz8uZGqsoLFz9I2w9au9CcRawJVW85qwX52', '093 78 18 78', 'Male', '1992-01-08', 'Snoul Kpos village, Kombol commune,Kandal Provice', 'Snoul Kpos village,Sangkat Kombol,Khan Porsenchey, Phnom Penh', '', 1, '2018/09/29', NULL, 'OdMgEvG4WGxDjVv1J6or0FpBpIeW3Ij2O9chdnHMyTqmKyZszKhrO8q9PRVZ', '2018-09-28 22:09:23', '2018-09-30 21:31:22'),
(632, 'HUOR PANHA', 2, 'panha.planb@gmail.com', '$2y$10$HcbOpR60nzZUSZchWc3IVOB7KT7U/GRVRtbod4MY/AcJxhLkqum0u', '098 99 62 98', 'Male', NULL, NULL, NULL, '', 0, '2018/10/17', NULL, 'zWytHTSHdQ3qKSZtrmsWvGUFyKbsEVdoVG3cpovIOGkXZUhZh8XTkngUIQs0', '2018-10-16 18:00:26', '2018-12-06 21:20:17'),
(633, 'CHHAY SOTHEARA', 2, 'chhaysotheara.planb@gmail.com', '$2y$10$E/a1Q6Jrv6kXm.KgIkZDnepssBi22vFq0acgSKxuXrZJ9oSNfmhIe', '098 993 369', 'Male', '1995-02-11', 'Phnom Penh', '#358, st. 590 Toul Kork Russey Keo', '', 1, '2018/12/03', NULL, 'DIn2qqwfK1iH78MmKMzi5SzlqV9rwWSTEjXOgo7swDfNM5STtXpAgQ7P7ynR', '2018-12-02 21:33:41', '2018-12-02 21:59:21'),
(634, 'MATH RORPHEEYAH', 2, 'rorpheeyah.planb@gmail.com', '$2y$10$cbK/QP6n/MCxc0rzLPqQle3q1HPyBcdD6Z8PSM.2PiQEBbtUeo.Zq', '085 524 103', 'Female', '1997-09-15', 'Kompong Chhnang', 'Toul Sangke, Phnom Penh', '', 1, '2018/12/03', NULL, 'NNaDzJzQUTNf5RInDM5X3brnbOY3YjTZbCRHG6NvLOAab2a7k4eROfIRcpCn', '2018-12-02 21:35:41', '2018-12-02 22:01:53'),
(635, 'RA CHANRITH', 2, 'chanrith.planb@gmail.com', '$2y$10$KQ8fpnixYM30PQOFUOz1E.4g6X7XtiePoiK/LSaOY0ytQ7WDH2Jsm', '010 27 42 62', 'Male', '1996-05-13', 'Kandal', 'Phnom Penh', '15438845352852074.rsz.jpg', 1, '2018/12/03', NULL, 'byQzYOQRqnIwoz1E47WxYc1w5ioj9co5VB5R6tQxNWutVYoaA518kaiWWuze', '2018-12-02 21:36:28', '2018-12-03 17:48:55'),
(636, 'HOENG LYHEANG', 2, 'lyheang.planb@gmail.com', '$2y$10$taEyQyGXwaeDGtghwGsiXuTNn85y8pTccPMXrOaDxADHP2jB34b7O', '081 666 856', 'Male', NULL, '30/jun/1994', 'st 93z songkat Phsar Daem Thkov, khan chom kar mun, phnom penh', '', 1, '2018/12/03', NULL, 'ATdGdeFbxDALOt5pGhPkkyMtnzHETRMwWisEgEYhDwmbuGtQPwvazsKCF3rj', '2018-12-02 21:37:21', '2018-12-03 19:26:53'),
(637, 'AM SAMOL', 2, NULL, '$2y$10$CGZOsfD2I2iFB5VPdCnacepsCbxqEfRvI0bWGZ4O2irnPVaFpYfGe', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2018-12-06 02:28:07', '2018-12-06 02:28:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `salaries`
--
ALTER TABLE `salaries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `salaries`
--
ALTER TABLE `salaries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=638;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
