<?php
	Route::group(['middleware'=>'admin'],function(){
		Route::get('dashboard-admin',[
			'uses'=>'LoginController@getAdminDashboard',
			'as'=>'dashboard.admin'
		]);
		Route::get('list-all-user',[
			'uses'=>'AdminController@getUser',
			'as'=>'user.list'
		]);

		Route::get('user-status/{id}',[
		    'uses'=>'AdminController@getStatusUpdate',
            'as'=>'user.status'
        ]);
		Route::get('user-edit/{id}',[
			'uses'=>'AdminController@getEditUser',
			'as'=>'user.edit'
		]);
		Route::get('delete-user/{id}',[
			'uses'=>'AdminController@getDeleteUser',
			'as'=>'delete.user'
		]);

		Route::get('view-user/{id}',[
			'uses'=>'AdminController@getViewUser',
			'as'=>'view.user'
		]);
		Route::get('add-user',[
			'uses'=>'AdminController@getAddUser',
			'as'=>'add.user'
		]);

		Route::post('add-user',[
			'uses'=>'AdminController@postAddUser',

		]);
		Route::post('update-user',[
			'uses'=>'AdminController@getUpdateUser',
			'as'=>'userUpdate'
		]);

		Route::get('search-user',[
			'uses'=>'AdminController@getSearchUser',
			'as'=>'user.searchUser'
		]);

		Route::post('add-user-csv',[
			'uses'=>'AdminController@getAddUserCSV',
			'as'=>'addUserCSV'
		]);

		Route::get('role',[
			'uses'=>'AdminController@getRole',
			'as'=>'user.role'
		]);


		Route::get('role-view/{id}',[
			'uses'=>'AdminController@getViewRole',
			'as'=>'user.roleView'
		]);

		Route::get('role-edit/{id}',[
			'uses'=>'AdminController@getRoleEdit',
			'as'=>'user.roleEdit'
		]);
		Route::get('role-delete/{id}',[
			'uses'=>'AdminController@getDeleteRole',
			'as'=>'user.roleDelete'
		]);

		Route::post('role-update',[
			'uses'=>'AdminController@postUpdateRole',
			'as'=>'user.updateRole'
		]);
		Route::get('role-create',[
			'uses'=>'AdminController@getCreateRole',
			'as'=>'roleCreate'
		]);
        Route::post('role',[
            'uses'=>'AdminController@postCreateRole',
        ]);
		Route::get('salary',[
			'uses'=>'AdminController@getSalary',
			'as'=>'user.salary'
		]);
		Route::get('create-salary',[
			'uses'=>'AdminController@getCreateSalary',
			'as'=>'user.createSalary'
		]);
		Route::post('create-salary',[
			'uses'=>'AdminController@postCreateSalary',
			'as'=>'user.createSalary'
		]);

		Route::post('upload-salary-csv',[
			'uses'=>'AdminController@postUploadSalaryCSVFile',
			'as'=>'salary.csv'
		]);
		Route::get('admin-view-salary-user/{id}',[
			'uses'=>'AdminController@getViewSalary',
			'as'=>'user.view.salary'
		]);
		Route::get('user-salary-detail/{id}',[
		    'uses'=>'AdminController@getSalaryDetail',
            'as'=>'user.salary.detail'
        ]);

		Route::get('edit-salary/{id}',[
		    'uses'=>'AdminController@getEditSalary',
            'as'=>'user.edit.salary'
        ]);
        Route::get('delete-salary/{id}',[
            'uses'=>'AdminController@getDeleteSalary',
            'as'=>'user.delete.salary'
        ]);
        Route::post('update-salary-user/',[
            'uses'=>'AdminController@postUpdateSalaryUser',
            'as'=>'user.updateSalary'
        ]);
	});

	Route::group(['middleware'=>'guest'],function(){
		Route::get('/login',[
			'uses'=>'LoginController@getLogin',

			'as'=>'user.login'
		]);


		Route::post('/login',[
			'uses'=>'LoginController@postLogin'
		]);
		Route::get('forgot-password',[
			'uses'=>'LoginController@getUserForgotPassword',
			'as'=>'user.forgotPassword'
		]);
		Route::post('forgot-password',[
			'uses'=>'LoginController@postUserForgotPassword',
			'as'=>'user.forgotPassword'
		]);
	});

	Route::get('reset-password/key/{key}',[
	    'uses'=>'LoginController@getResetPassword',
        'as'=>'reset-password'
    ]);

	Route::get('reset-new-password',[
	    'uses'=>'LoginController@getUpdateResetNewPassword',
        'as'=>'reset-new-password'
    ])->middleware('auth');

	Route::post('user.update-password',[
	    'uses'=>'LoginController@postUpdateNewPassword',
        'as'=>'user.updatePassword'
    ]);

    Route::get('dashboard-user',[
        'uses'=>'LoginController@getUserDashboard',
        'as'=>'dashboard.user'
    ]);
    Route::get('view-salary/{id}',[
        'uses'=>'AdminController@getViewSalaryNormal',
        'as'=>'view.salary.user.normal'
    ]);

    Route::get('normal-user',[
        'uses'=>'AdminController@getNormalUserSalary',
        'as'=>'normalUser.salary'
    ]);

	Route::get('logout',[
		'uses'=>'LoginController@getLogout',
		'as'=>'user.logout'
	]);

	Route::group(['middleware'=>'auth'],function (){
        Route::get('update-user-profile/{id}',[
            'uses'=>'AdminController@getUserUpdateProfile',
            'as'=>'user.update.profile'
        ]);
        Route::post('update-user-profile',[
            'uses'=>'AdminController@poseUpdateProfile',
            'as'=>'user.update.profiles'
        ]);
        Route::get('normal-user-update-profile/{id}',[
            'uses'=>'AdminController@getUpdateUserProfiles',
            'as'=>'user.profile'
        ]);
    });

	Route::get('/',function(){
	    return redirect()->route('user.login');
    });

	Route::get('/test',[
	    'uses'=>'AdminController@getResponse',
        'as'=>'response'
    ]);
?>