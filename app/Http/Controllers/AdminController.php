<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Salary;
use App\Models\User;
use DB;
use Excel;
use Illuminate\Http\Request;
use Session;

class AdminController extends Controller
{
    public function getUser()
    {
        $users = User::all();
        return view('admin.user')->withUsers($users);
    }

    public function getStatusUpdate($id){
        $user=User::find($id);
        if(!empty($user)){
            if($user->status=="1"){
                $user->status="0";
            }else{
                $user->status="1";
            }
            $user->save();
        }
        return redirect()->back()->withInput()->withErrors(['notice'=>'User has been updated']);
    }
    public function getSearchUser(Request $request)
    {
        $users = User::where('name', 'LIKE', '%' . $request->search . '%')->get();
        return view('admin.user')->withUsers($users);
    }

    public function getEditUser($id)
    {
        $user = User::find($id);
        $roles = Role::all();
        return view('admin.editUser')->withUser($user)->withRoles($roles);;
    }

    public function getDeleteUser($id)
    {
        $user = User::find($id);
        DB::table('salaries')->where('user_id',$id)->delete();
        $user->delete();
        return redirect()->back()->withInput()->withErrors(['notice'=> 'User has been removed']);
    }

    public function getAddUser()
    {
        $roles = Role::all();
        return view('admin.addUser')->withRoles($roles);
    }

    public function postAddUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);
        $fileName = "";
        if (!empty($request->file('picture'))) {
            $file = $request->file('picture');
            $fileName = $file->getClientOriginalName();
            $fileName = time() . $fileName;
            $file->move('uploads/', $fileName);
        }
        $user = new User();
        $user->name = $request->name;
        $user->role_id = $request->role_id;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->phone = $request->phone;
        $user->sex = $request->sex;
        $user->dob = $request->dob;
        $user->pob = $request->pob;
        $user->address = $request->address;
        $user->picture = $fileName;
        $user->status = $request->status;
        $user->register_date = date("Y/m/d");
        $user->save();
        return redirect()->back()->withInput()->withErrors(['notice' => 'User has been created']);

    }

    public function getUpdateUser(Request $request)
    {
        $user = User::find($request->id);
        $fileName = "";
        if (!empty($request->file('picture'))) {
            $file = $request->file('picture');
            $fileName = $file->getClientOriginalName();
            $fileName = time() . $fileName;
            $file->move('uploads/', $fileName);
        }
        $user->name = $request->name;
        $user->role_id = $request->role_id;
        $user->email = $request->email;
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }
        if (!empty($fileName)) {
            $user->picture = $fileName;
        }
        $user->phone = $request->phone;
        $user->sex = $request->sex;
        $user->dob = $request->dob;
        $user->pob = $request->pob;
        $user->address = $request->address;
        $user->status = $request->status;
        $user->register_date = date("Y/m/d");
        $user->save();
        return redirect()->route('user.list')->withInput()->withErrors(['notice' => 'User has been upated']);
    }

    public function getViewSalary($id)
    {
        $salary = Salary::find($id);
        return view('admin.viewSalary')->with('salary', $salary);
    }

    public function getAddUserCSV(Request $request)
    {
        $this->validate($request, [
            'file' => 'required'
        ]);
        $file = $request->file('file');
        $fileName = $file->getClientOriginalName();
        $fileExtention = explode(".", $fileName);
        $fileExtention = end($fileExtention);
        if ($fileExtention != "csv") {
            return redirect()->back()->withInput()->withErrors(['error' => 'File extention not valid (csv file)']);
        }
        $errors=null;
        $userFound=null;
        Session::forget('userFound');
        Session::forget('notFound');
        Excel::load($request->file('file'), function ($reader) {
            $reader->each(function ($sheet) {
                global $errors;
                global $userFound;
                $checkExist=User::where('email',$sheet->email)->first();
                if(!$checkExist){
                     $user = new User();
                     $user->name = $sheet->name;
                     $user->email = $sheet->email;
                     $user->phone = $sheet->phone;
                     $user->password = bcrypt('12345');
                     $user->save();
                     $userFound.="<li>".$sheet->email."</li>";
                     Session::put('userFound',$userFound);
                }else{
                    $userFound.="<li>".$sheet->email."</li>";
                    Session::put('notFound',$userFound);
                }
               
            });
        });
        return redirect()->back()->withInput()->withErrors(['notices' => 'User file has beeen uploaded','Exists'=>Session::get('notFound'),'Adds'=>Session::get('userFound')]);
    }

    public function getRole()
    {
        $roles = Role::all();
        return view('admin.role')->withRoles($roles);
    }

    public function getViewRole($id)
    {
        $role = Role::find($id);
        return view('admin.viewRole')->withRole($role);
    }

    public function getRoleEdit($id)
    {
        $role = Role::find($id);
        return view('admin.editRole')->withRole($role);
    }

    public function postUpdateRole(Request $request)
    {
        $id = $request->id;
        $name = $request->name;
        $description = $request->description;
        $role = Role::find($id);
        $role->name = $name;
        $role->description = $description;
        $role->save();
        return redirect()->route('user.role')->withInput()->withErrors(['notice' => 'Role has been updated']);
    }

    public function postROle(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $role = Role::create($request->all());
        return redirect()->back()->withInput()->withErrors(['notice' => 'Role has been created']);
    }

    public function getDeleteRole($id)
    {
        $role = Role::find($id);
        $role->delete();
        return redirect()->route('user.role')->withInput()->withErrors(['notice' => 'Role has been deleted']);
    }

    public function getSalary(Request $request)
    {
        $numberOfUser = User::count();
        $salaries = Salary::where('status', '1')->whereIn('id', [DB::raw('(SELECT MAX(id) FROM salaries GROUP BY user_id)')])->groupBy('user_id')->paginate($numberOfUser);
        $users=User::where('status','1')->get();
        return view('admin.salary')->with('salaries', $salaries)->with('users',$users);
    }

    public function getCreateSalary()
    {
        $users = User::where('status', '1')->get();
        return view('admin.createSalary')->withUsers($users);
    }

    public function getSalaryDetail($id)
    {
        $user = User::find($id);
        return view('admin.salaryDetail')->with('user', $user);
    }

    public function postCreateSalary(Request $request)
    {
        $this->validate($request, [
            'month' => 'required',
            'salary' => 'required',
            'basic_salary' => 'required',
            'gasoline' => 'required'
        ]);
        $date = $request->month;
        $date = date("F-Y", strtotime($date));
        $date = explode("-", $date);
        $month = $date[0];
        $year = $date[1];
        $salary = new Salary();
        $salary->user_id = $request->user_id;
        $salary->month = $month;
        $salary->year = $year;
        $salary->basic = $request->basic_salary;
        $salary->rate = $request->rate;
        $salary->no_of_day = $request->no_of_day;
        $salary->basic_salary = $request->basic_salary;
        $salary->gasoline = $request->gasoline;
        $salary->attendance = $request->attendance;
        $salary->extra_plus = $request->extra_plus;
        $salary->minus = $request->minus;
        $salary->salary_tax = $request->salary_tax;
        $salary->tax = $request->tax;
        $salary->salary = $request->salary;
        $salary->save();
        return redirect()->back()->withInput()->withErrors(['notice' => 'Salary has been added']);

    }

    public function postUpdateSalaryUser(Request $request)
    {
        $salary = Salary::find($request->id);
        $salary->user_id = $request->user_id;
        $salary->month = $request->month;
        $salary->year = $request->year;
        $salary->basic = $request->basic_salary;
        $salary->rate = $request->rate;
        $salary->no_of_day = $request->no_of_day;
        $salary->basic_salary = $request->basic_salary;
        $salary->gasoline = $request->gasoline;
        $salary->attendance = $request->attendance;
        $salary->extra_plus = $request->extra_plus;
        $salary->minus = $request->minus;
        $salary->salary_tax = $request->salary_tax;
        $salary->tax = $request->tax;
        $salary->salary = $request->salary;
        $salary->save();
        return redirect()->route('user.salary')->withInput()->withErrors(['notice' => 'Salary has been updated']);
    }

    public function getEditSalary($id)
    {
        $salary = Salary::find($id);
        $users = User::where('status', '1')->get();
        return view('admin.editSalary')->with('salary', $salary)->with('users', $users);
    }

    public function getDeleteSalary($id)
    {
        $salary = Salary::find($id);
        $salary->delete();
        return redirect()->back()->withInput()->withErrors(['notice' => 'Salary has been deleted']);
    }

    public function getUserUpdateProfile($id)
    {
        $user = User::find($id);
        return view('admin.updateProfile')->with('user', $user);
    }

    public function poseUpdateProfile(Request $request)
    {

        $user = User::find($request->id);
        $fileName = "";
        if (!empty($request->file('picture'))) {

            $file = $request->file('picture');
            $fileName = $file->getClientOriginalName();
            $fileName = time() . $fileName;
            $file->move('uploads/', $fileName);
        }
        $user->name = $request->name;
        $user->email = $request->email;
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }
        if (!empty($fileName)) {
            $user->picture = $fileName;
        }
        $user->phone = $request->phone;
        $user->sex = $request->sex;
        $user->dob = $request->dob;
        $user->pob = $request->pob;
        $user->address = $request->address;
        $user->save();
        return redirect()->back()->withInput()->withErrors(['notice' => 'User has been updated']);
    }

    public function postUploadSalaryCSVFile(Request $request)
    {
        $this->validate($request, [
            'file' => 'required'
        ]);
        $file = $request->file('file');
        $fileName = $file->getClientOriginalName();
        $fileExtension = explode(".", $fileName);
        $fileExtension = end($fileExtension);
        $errors=null;
        $userFound=null;
        Session::forget('userFound');
        Session::forget('notFound');
        if ($fileExtension != "csv") {
            return redirect()->back()->withInput()->withErrors(['error' => 'File extention not valid (csv file)']);
        }
        Excel::load($request->file('file'), function ($reader) {
            $reader->each(function ($sheet) {
                global $errors;
                global $userFound;
                if (!empty(User::where('name', $sheet->name)->first()->id)) {
                    $userFound.=$sheet->name."|";
                    Session::put('userFound',$userFound);
                }
                if(empty(User::where('name', $sheet->name)->first()->id)) {
                    $errors.=$sheet->name."|";
                    Session::put('notFound',$errors);
                }
                try {
                    $id = User::where('name', $sheet->name)->first()->id;
                    $salary = new Salary();
                    $salary->user_id = $id;
                    $date = $sheet->month;
                    $date = explode(",", $date);
                    $month = $date[0];
                    $year = $date[1];
                    $salary->month = $month;
                    $salary->year = $year;
                    $salary->basic = str_replace("$", "", $sheet->basic);
                    $salary->minus = str_replace("$", "", $sheet->minus);
                    $salary->rate = str_replace("$", "", $sheet->rate);
                    $salary->no_of_day = str_replace("$", "", $sheet->no_of_day);
                    $salary->basic_salary = str_replace("$", "", $sheet->basic_salary);
                    $salary->gasoline = str_replace("$", "", $sheet->gasoline);
                    $salary->attendance = str_replace("$", "", $sheet->attendance);
                    $salary->extra_plus = str_replace("$", "", $sheet->extra_plus);
                    $salary->salary_tax = str_replace("$", "", $sheet->salary_tax);
                    $salary->tax = str_replace("$", "", $sheet->tax);
                    $salary->salary = str_replace("$", "", $sheet->salary);
                    $salary->save();
                } catch (\Exception $ex) {
                    Session::put('error', $ex->getMessage());
                }
            });

        });
       return redirect()->back()->withInput()->withErrors(['notFound'=>Session::get('notFound'),'userFound'=>Session::get("userFound")]);
    }

    public function getViewSalaryNormal($id)
    {
        $salary = Salary::find($id);
        return view('admin.normalSalary')->with('salary', $salary);
    }

    public function getUpdateUserProfiles(Request $request, $id)
    {
        $user = User::find($id);
        return view('admin.updateProfileUser')->with('user', $user);
    }

    public function getCreateRole(){
        return view(' admin.roleCreate');
    }
    public function postCreateRole(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:roles',
            'description'=>'required'
        ]);
        $name=$request->name;
        $description=$request->description;
        $role=new Role();
        $role->name=$name;
        $role->description=$description;
        $role->save();
        return redirect('/role')->withInput()->withErrors(['notice'=>'Role has been created']);
    }


}
