<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Mail;
class LoginController extends Controller
{
	  use AuthenticatesUsers;
    /*
		get login 
    */
		public function getLogin(){
			return view('login');
		}
	/*
		post login
	*/
		public function postLogin(Request $request){
			$this->validate($request,[
				'email'=>'required|exists:users',
				'password'=>'required'
			]);
			if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
			    if(Auth::user()->status!=1){
			        Auth::logout();
                    return redirect()->back()->withInput()->withErrors(['email'=>'Your account has been disabled']);

                }
				if(!empty(Auth::user()->role)){
					if(strtolower(Auth::user()->role->name)=='admin'){
						return redirect()->route('dashboard.admin');
					}
					return redirect()->route('dashboard.user');
				}
                return redirect()->route('dashboard.user');
			}else{
				return redirect()->back()->withInput()->withErrors(['email'=>'Incorrect email or password']);
			}
		}

		public function getUserForgotPassword(){
			return view('reset');
		}

		public function postUserForgotPassword(Request $request){
			$this->validate($request,[
				'email'=>'required',
			]);
			$email=$request->email;
			$user=User::where('email',$email)->first();
			if(empty($user)){
			   return redirect()->route('user.forgotPassword')->withInput()->withErrors(['email'=>'Your email not exist in database']);
            }

            $key=rand(1,999999);
            $key=md5($key);
            $user->key=$key;
            $user->save();
            $data = array(
                'email' => $email,
                'key' => $key

            );
            Mail::send('email.verify_email', $data, function ($m) use ($email) {
                $m->from('channvuthy.plan.b@gmail.com', 'Plan-B HR');
                $m->to($email, '')->subject('Change New Password');
            });
            return redirect()->back()->withInput()->withErrors(['email'=>'Please check your email address']);
		}

		public function getResetPassword($key){
           $user=User::where('key',$key)->first();
           if(!empty($user)){
                Auth::login($user);
                return redirect()->route('reset-new-password');
           }else{
               return redirect()->route('user.forgotPassword')->withInput()->withErrors(['email'=>'Your key is  incorrect']);
           }
        }

        public function getUpdateResetNewPassword(){
		    return view('updatePassword');
        }

        public function postUpdateNewPassword(Request $request){
            $this->validate($request,[
                'password'=>'required'
            ]);
            $user=User::find(Auth::user()->id);
            $user->password=bcrypt($request->password);
            $user->save();
            Auth::logout();
            return redirect()->route('user.login');
        }

		public function getAdminDashboard(){
			return view('admin.dashboard');
		}

		public function getUserDashboard(){
		    return view('admin.normalUser');
		}

		public function getLogout(){
			Auth::logout();
			return redirect()->route('user.login');
		}
}
