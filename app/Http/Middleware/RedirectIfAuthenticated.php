<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
           if(!empty(Auth::user()->role)){
                if(strtolower(Auth::user()->role->name)=='admin'){
                    return redirect()->route('dashboard.admin');
                }
           }
           return redirect()->route('dashboard.user');
        }

        return $next($request);
    }
}
