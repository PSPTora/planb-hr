<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(strtolower(Auth::user()->role->name)=='user'){
                return $next($request);
            }
            return response("Insufficient Permission");
        }else{
            return redirect()->route('user.login');
        }
    }
}
