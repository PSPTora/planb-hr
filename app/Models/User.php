<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        return $this->hasOne('App\Models\Role','id','role_id');
    }

    public function salaries(){
       // return $this->hasMany("App\Models\Salary","user_id")->orderBy('id','DESC')->groupBy('year')->groupBy('month');
        return $this->hasMany("App\Models\Salary","user_id")->orderBy('id','DESC');
    }
    public function salary(){
        return $this->hasMany("App\Models\Salary","user_id");

    }

}
