-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2017 at 05:39 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hr`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pob` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `register_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `role_id`, `email`, `password`, `phone`, `sex`, `dob`, `pob`, `address`, `picture`, `status`, `register_date`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'chann vuthy', 1, 'channvuthyit@gmail.com', '$2y$10$ZbRhBcaLzrikLCY0eG/FNevzeyGVNl0ojv9KP5BdqH3KVJr7d9D.W', '098909471', 'Male', '2017-05-16', 'Takeo', 'Phnom Penh, Tuek Thla, Ou Baek K\'am', '149862112512048661_1675786919366696_1645854904_n.jpg', 1, '2017/05/03', 'HPTcSuae5GvBa2wr8WLk8eMOiC26ugU6EIwbHXB72w7sDgPhphzM3KVgeyIG', NULL, '2017-06-27 20:38:45'),
(453, 'Chann Phalla', 1, 'channphalla@gmail.com', '$2y$10$U4utmd6wxbFU0UV1ZBjA4eTGlKXiCQ4Q.CGRiV7n1wVhzzfgA4Csm', NULL, NULL, NULL, NULL, NULL, '', 1, '2017/06/28', 'v2zMPkkJyHevKUJAPnThZfT2nndzZxxGssahvXDYQ51vJ62L7E0uH3diz2Li', '2017-06-27 19:21:04', '2017-06-27 19:22:37'),
(454, 'chann dara', NULL, 'channdara@gmail.com', '$2y$10$SKibjfP9XIyPYTKGER7Nhu9YcbMM5o/UEHP4T2Ua3HB0Lrwk337M.', '098909471', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'CwPQSSzFAiXjcGESBpI51blzc3h16vKarKqtWNOUp7eJH88qQHJzVDHXyvoc', '2017-06-27 19:25:11', '2017-06-27 19:25:11'),
(455, 'meas phalla', NULL, 'meaphalla@gmail.com', '$2y$10$DE99ZBruZDXJ5xeFqpcCiOpYG4SE6e6vxhHnfs9LuLx/vS192vgoK', '098909472', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'LcNFHyxRaEBhOODf33fvSVCwB0DZk5HNpBKZjnHbOdr9e2OiZBNRjFp4YLz7', '2017-06-27 19:25:11', '2017-06-27 19:25:11'),
(456, 'orthaybona', NULL, 'orthaybona@gmail.com', '$2y$10$4b5sMSfax/Y5F8BY3RlKn.wsprD8Y4rVcC5HKtHS0pRoJZZK1Rsiq', '098909473', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-06-27 19:25:11', '2017-06-27 19:25:11'),
(457, 'meas saray', NULL, 'measavary@gmail.com', '$2y$10$3uuVvaBsDgpufJIRC4qKkepcvaS0FEIMWfXCFxVcnG2Tx3nGjYbTy', '098909474', NULL, NULL, NULL, NULL, '149862063913082691_1594646704186221_4515546799425266654_n.jpg', 1, NULL, 'ItujHZ8oXbGTxkrUyBLXlJE5Uf1huHb4vTLAYl6s9fXXUW6BoEgosb5GFlwM', '2017-06-27 19:25:11', '2017-06-27 20:30:39'),
(458, 'panh nha', NULL, 'panha@gmail.com', '$2y$10$F3p/TLwcG..rMsRACfIDw.j0ZgTt3Oon/v01UBOaMU61nStTQuDIC', '098909475', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-06-27 19:25:11', '2017-06-27 19:25:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=459;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
